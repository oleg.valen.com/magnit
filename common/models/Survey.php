<?php


namespace common\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use Yii;

class Survey extends ActiveRecord
{
    //GM - директор ГиперМаркета
    //BF - директор группы Больших Форматов
    //RMKS - Региональный Менеджер по Клиентским Сервисам

    const STATUS_INIT = 0; //any new survey
    const STATUS_NEW = 10; //from this till the closed - has alert

    const STATUS_GM_EXPIRED = 20;
    const STATUS_GM_TAKEN = 30;
    const STATUS_GM_CLOSED = 40;

    const STATUS_BF_REVISION = 50;
    const STATUS_BF_CLOSED = 60;

    const STATUS_CLASSES = [
        self::STATUS_INIT => 'SurveyStatusInit',
        self::STATUS_NEW => 'SurveyStatusNew',
        self::STATUS_GM_EXPIRED => 'SurveyStatusGMExpired',
        self::STATUS_GM_TAKEN => 'SurveyStatusGMTaken',
        self::STATUS_GM_CLOSED => 'SurveyStatusGMClosed',
        self::STATUS_BF_REVISION => 'SurveyStatusBFRevision',
        self::STATUS_BF_CLOSED => 'SurveyStatusBFClosed',
    ];

    const BAD_NPS = [0, 1, 2, 3];
    const PASSIVE_NPS = [4];
    const GOOD_NPS = [5];

    public static $surveys = [
        SurveyType::CASH => [
            1 => 'Оцените доброжелательность кассира',
            2 => 'Оцените скорость облуживания на кассе',
        ],
        SurveyType::FRUIT => [
            1 => 'Оцените свежесть товаров в Отделе Фрукты и овощи',
            2 => 'Оцените чистоту в Отделе Фрукты и овощи',
        ],
        SurveyType::PROD => [
            1 => 'Оцените качество товаров в отделе Собственного производства',
            2 => 'Оцените ассортимент товаров в отделе Собственного производства',
            3 => 'Оцените доброжелательность продавца в отделе Собственного производства',
        ],
        SurveyType::SERVICE => [
            1 => 'Решили ли сотрудники Ваш вопрос?',
            2 => 'Стремились ли сотрудники помочь Вам?',
        ],
        SurveyType::SHOPPING => [
            1 => 'Оцените дружелюбность и отзывчивость персонала',
            2 => 'Оцените ассортимент в магазине',
            3 => 'Насколько Вы готовы рекомендовать данный магазин?',
        ],
    ];

    public static $statusValues = [
//    const STATUS_INIT = 0; //any new survey
//    const STATUS_NEW = 10; //from this till the closed - has alert
//
//    const STATUS_GM_TAKEN = 20;
//    const STATUS_GM_EXPIRED = 30;
//    const STATUS_GM_CLOSED = 40;
//
//    const STATUS_BF_REVISION = 50;
//    const STATUS_BF_CLOSED = 60;
        Survey::STATUS_INIT => 'Init',
        Survey::STATUS_NEW => 'Новый',
        Survey::STATUS_GM_TAKEN => 'Взят в работу',
        Survey::STATUS_GM_EXPIRED => 'Просрочен',
        Survey::STATUS_GM_CLOSED => 'На подтверждении',
        Survey::STATUS_BF_REVISION => 'Взят в работу',
        Survey::STATUS_BF_CLOSED => 'Закрыт',
    ];

    protected $oldStatus;
    public $imageFiles = [];

    public static function tableName()
    {
        return '{{survey}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
//                // если вместо метки времени UNIX используется datetime:
//                'value' => new Expression('NOW()'),
            ],
        ];
    }

    public function rules()
    {
        return [
//            [['client_id', 'doc_id'], 'required'],
            [['survey_id', 'client_id', 'store_id', 'dep_id', 'nps', 'question1', 'question2', 'question3', 'question4'], 'integer'],
            [['survey_id', 'client_id', 'store_id', 'dep_id', 'nps', 'question1', 'question2', 'question3', 'question4',
                'comment', 'status', 'status_updated_at', 'created_at', 'updated_at'], 'safe'],
            [['imageFiles'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg', 'maxFiles' => 4],
        ];
    }

    public function afterFind()
    {
        $this->oldStatus = $this->status;
        return parent::afterFind();
    }

    public function beforeSave($options = [])
    {
        if ($this->status != $this->oldStatus)
            $this->status_updated_at = time();

        return parent::beforeSave($this->isNewRecord);
    }

    public function upload()
    {
        if ($this->validate()) {
            foreach ($this->imageFiles as $file) {
                $path = Yii::getAlias('@frontend') . '/web/uploads/' . $this->survey_id . '_' . $file->baseName . '.' . $file->extension;
                $file->saveAs($path);
                @chmod($path, 0755);
            }
            return true;
        } else {
            return false;
        }
    }

    public function getStore()
    {
        return $this->hasOne(Store::className(), ['id' => 'store_id']);
    }

    public function getDepartment()
    {
        return $this->hasOne(Department::className(), ['id' => 'dep_id']);
    }

    public function getDeliveryDoc()
    {
        return $this->hasOne(DeliveryDoc::className(), ['doc_id' => 'doc_id']);
    }

    public function getClient()
    {
        return $this->hasOne(Client::className(), ['client_id' => 'client_id']);
    }

    public function getLogs()
    {
        return $this->hasMany(Log::className(), ['survey_id' => 'survey_id']);
    }

}
