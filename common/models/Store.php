<?php

namespace common\models;

use yii\db\ActiveRecord;

class Store extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%store}}';
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'area_id' => 'ID региона',
            'name' => 'Магазин',
        ];
    }
}
