<?php

namespace common\models;

use yii\db\ActiveRecord;

class Session extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%session}}';
    }
}
