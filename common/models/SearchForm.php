<?php

namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Search form
 */
class SearchForm extends Model
{
    public $search;
    public $createdAt;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['search'], 'required'],
            [['createdAt'], 'safe'],
        ];
    }

}
