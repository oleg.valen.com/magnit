<?php


namespace common\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

class Log extends ActiveRecord
{

    const TYPE_RETAIL = 1;
    const TYPE_DELIVERY = 2;
    const TYPE_B2B = 3;
    const TYPE_ASC = 4;

    public static $statuses = [
        Survey::STATUS_GM_TAKEN => 'Взял в работу',
        Survey::STATUS_GM_CLOSED => 'Закрыл Алерт',
        Survey::STATUS_BF_CLOSED => 'Алерт принят',
        Survey::STATUS_BF_REVISION => 'Алерт на доработку',
        //todo доделать логирование по другим статусам при необходимости
    ];

    public static function tableName()
    {
        return '{{log}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
//                // если вместо метки времени UNIX используется datetime:
//                'value' => new Expression('NOW()'),
            ],
        ];
    }

    public function rules()
    {
        return [
            [['type_id', 'survey_id'], 'required'],
            [['type_id', 'survey_id', 'user_id', 'status'], 'integer'],
            [['user_id', 'comment', 'status', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

}
