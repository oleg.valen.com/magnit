<?php


namespace common\models\SurveyStatus;


use common\models\Survey;

class SurveyContext
{
    private $survey;
    private $surveyStatus;

    public function __construct(Survey $survey, SurveyStatus $surveyStatus)
    {
        $this->transitionTo($survey, $surveyStatus);
    }

    public function transitionTo(Survey $survey, SurveyStatus $surveyStatus)
    {
        $this->survey = $survey;
        $this->surveyStatus = $surveyStatus;
        $this->surveyStatus->setContext($this);
    }

    public function getSurvey()
    {
        return $this->survey;
    }

    public function handle()
    {
        $this->surveyStatus->handle();
    }

}
