<?php


namespace common\models\SurveyStatus;


use common\models\Survey;

class SurveyStatusNew extends SurveyStatus
{
    protected $nextStatus = Survey::STATUS_GM_EXPIRED;
//    protected $levels = [1, 3];
    protected $subject = 'Новый алерт';
    protected $body = "В системе появился новый алерт\n";
}
