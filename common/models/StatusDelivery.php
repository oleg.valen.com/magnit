<?php

namespace common\models;

use yii\db\ActiveRecord;

class StatusDelivery extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%status_delivery}}';
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Статус доставки',
        ];
    }
}
