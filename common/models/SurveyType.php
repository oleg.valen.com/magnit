<?php

namespace common\models;

class SurveyType
{
    const CASH = 1;
    const FRUIT = 2;
    const PROD = 3;
    const SERVICE = 4;
    const SHOPPING = 5;

    public static $values = [
        self::CASH => 'cash',
        self::FRUIT => 'fruit',
        self::PROD => 'prod',
        self::SERVICE => 'service',
        self::SHOPPING => 'shopping',
    ];

    public static $valueNames = [
        self::CASH => 'Касса',
        self::FRUIT => 'Фрукты и овощи',
        self::PROD => 'Собственное производство',
        self::SERVICE => 'Сервисная стойка',
        self::SHOPPING => 'Покупательские шкафчики',
    ];
}
