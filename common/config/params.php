<?php
return [
    'adminEmail' => 'admin@example.com',
//    'supportEmail' => 'no-reply@shopperbonus4service.com',
    'supportEmail' => 'inventive-opros@4service.com.ru',
    'senderEmail' => 'inventive-opros@4service.com.ru',
    'noreplyEmail' => 'saycoffeemania@4service-group.com',
//    'noreplyEmail' => 'guesttrack.supp@gmail.com',
    'senderName' => 'Inventive Support',
    'receiverEmail' => 'oleg.valen.com@gmail.com',
    'receiverName' => 'Oleh',
    'user.passwordResetTokenExpire' => 86400,
];
