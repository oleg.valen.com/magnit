<?php

namespace common\services;

use common\models\ClientToken;
use common\models\Department;
use common\models\SurveyType;
use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\httpclient\Client;

class GoogleChartService
{
    const ROOT_URL = 'https://chart.googleapis.com/chart?';

    public function create()
    {
        $file = Yii::getAlias('@frontend') . '/web/uploads/qr/'; //todo 1 check

        $get = array(
            'cht' => 'qr',
            'chs' => '300x300',
        );

        foreach (ClientToken::find()->all() as $token) {
            $ch = curl_init(self::ROOT_URL . http_build_query(array_merge($get, ['chl' => 'https://magnitopros.ru/' . $token->token])));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_HEADER, false);
            $result = curl_exec($ch);
            curl_close($ch);

            $fp = fopen($file . $token->store->name . '-' . SurveyType::$values[$token->dep_id] . '.png', 'w');
            fwrite($fp, $result);
            fclose($fp);
        }
    }

}
