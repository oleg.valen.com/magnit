<?php

namespace common\services;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\httpclient\Client;

class SmsService
{

    //https://smsc.ru/sys/send.php?login=alex&psw=123&phones=79999999999&mes=Hello!
    //https://smsc.ru/sys/send.php?login=alex&psw=123&list=79999999999:Hello!%0A79999999999:Hello\nworld!

    const DELIMETER = '%0A';
    const NEW_LINE = '\n';

    const SENT_STATUS_ERRORS = [
        1 => 'Ошибка в параметрах.',
        2 => 'Неверный логин или пароль. Также возникает при попытке отправки сообщения с IP-адреса, не входящего в список разрешенных Клиентом (если такой список был настроен Клиентом ранее).',
        3 => 'Недостаточно средств на счете Клиента.',
        4 => 'IP-адрес временно заблокирован из-за частых ошибок в запросах.',
        5 => 'Неверный формат даты.',
        6 => 'Сообщение запрещено (по тексту или по имени отправителя). Также данная ошибка возникает при попытке отправки массовых и (или) рекламных сообщений без заключенного договора.',
        7 => 'Неверный формат номера телефона.',
        8 => 'Сообщение на указанный номер не может быть доставлено.',
        9 => 'Отправка более одного одинакового запроса на передачу SMS-сообщения либо более пяти одинаковых запросов на получение стоимости сообщения в течение минуты. Данная ошибка возникает также при попытке отправки пятнадцати и более запросов одновременно с разных подключений под одним логином (too many concurrent requests).',
    ];

    protected $url;

    public function __construct($url)
    {
        $this->url = $url;
    }

    public function send($deliverySurvey)
    {
        $params = Yii::$app->params['sms'];

        //// for group recipients..
        //        $recipients = [
        //            [
        //                'phone' => '+380676384228',
        //                'message' => $params['message'],
        //                'url' => 'https://www.google.com.ua',
        //            ],
        //        ];
        //        $list = array_reduce($recipients, function ($list, $item) {
        //            $list .= $item['phone'] . ':' . $item['message'] . self::NEW_LINE . $item['url'] . self::DELIMETER;
        //            return $list;
        //        });
        //        $list = rtrim($list, self::DELIMETER);
        //        $query = [
        //            'login' => $params['login'],
        //            'psw' => $params['psw'],
        //            'list' => $list,
        //        ];

        $data = [
//            'phone' => '80676384228',
//            'phone' => '80664193242',
//            'phone' => '79774439514',
            'url' => 'https://www.google.com.ua',
        ];

        $query = [
            'login' => $params['login'],
            'psw' => $params['psw'],
            'phones' => Html::encode($deliverySurvey->client->phone),
//            'phones' => $data['phone'],
            'mes' => Html::encode($params['message'] . ' ' . $this->url),
        ];

        $client = new Client(['transport' => 'yii\httpclient\CurlTransport']); // only cURL supports the options we need
        $response = $client->createRequest()
            ->setMethod('GET')
            ->setFormat(Client::FORMAT_JSON)
            ->setUrl($params['url'] . http_build_query($query))
            ->send();

        //"OK - 1 SMS, ID - 1964805"
        if (preg_match('/(?<status>OK).+(ID - )(?<id>\d+)/', $response->content, $matches)) {
            $deliverySurvey->sms_sent = true;
            $deliverySurvey->sms_sent_id = $matches['id'];
            $deliverySurvey->save(false);
        }

        //"ERROR = 1 (parameters error)"
        if (preg_match('/ERROR = (?<id>\d+)/', $response->content, $matches)) {
            $deliverySurvey->sms_sent_status_error = $matches['id'];
            $deliverySurvey->save(false);
        }
    }

}
