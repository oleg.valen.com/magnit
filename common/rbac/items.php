<?php

return [
    'viewGM' => [
        'type' => 2,
        'description' => 'View GM',
    ],
    'viewBF' => [
        'type' => 2,
        'description' => 'View BF',
    ],
    'viewRMKS' => [
        'type' => 2,
        'description' => 'View RMKS',
    ],
    'GM' => [
        'type' => 1,
        'children' => [
            'viewGM',
        ],
    ],
    'BF' => [
        'type' => 1,
        'children' => [
            'viewBF',
        ],
    ],
    'RMKS' => [
        'type' => 1,
        'children' => [
            'viewRMKS',
        ],
    ],
    'fullRights' => [
        'type' => 1,
        'children' => [
            'GM',
            'BF',
            'RMKS',
        ],
    ],
    'admin' => [
        'type' => 1,
        'children' => [
            'fullRights',
        ],
    ],
];
