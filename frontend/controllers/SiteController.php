<?php

namespace frontend\controllers;

use common\models\Client;
use common\models\ClientToken;
use common\models\Department;
use common\models\Session;
use common\models\Survey;
use common\models\SurveyType;
use common\models\SurveyUploadForm;
use frontend\models\ResendVerificationEmailForm;
use frontend\models\VerifyEmailForm;
use Yii;
use yii\base\InvalidArgumentException;
use yii\helpers\Html;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

//    /**
//     * Displays contact page.
//     *
//     * @return mixed
//     */
//    public function actionContact()
//    {
//        $model = new ContactForm();
//        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
//            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
//                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
//            } else {
//                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
//            }
//
//            return $this->refresh();
//        }
//
//        return $this->render('contact', [
//            'model' => $model,
//        ]);
//    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post()) && $model->signup()) {
            Yii::$app->session->setFlash('success', 'Thank you for registration. Please check your inbox for verification email.');
            return $this->goHome();
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            }

            Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * Verify email address
     *
     * @param string $token
     * @return yii\web\Response
     * @throws BadRequestHttpException
     */
    public function actionVerifyEmail($token)
    {
        try {
            $model = new VerifyEmailForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if (($user = $model->verifyEmail()) && Yii::$app->user->login($user)) {
            Yii::$app->session->setFlash('success', 'Your email has been confirmed!');
            return $this->goHome();
        }

        Yii::$app->session->setFlash('error', 'Sorry, we are unable to verify your account with provided token.');
        return $this->goHome();
    }

    /**
     * Resend verification email
     *
     * @return mixed
     */
    public function actionResendVerificationEmail()
    {
        $model = new ResendVerificationEmailForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                return $this->goHome();
            }
            Yii::$app->session->setFlash('error', 'Sorry, we are unable to resend verification email for the provided email address.');
        }

        return $this->render('resendVerificationEmail', [
            'model' => $model
        ]);
    }

    public function actionSurvey()
    {
        $request = Yii::$app->request;
        $token = $request->get('token', null);
        $this->redirect(['admin/survey/create', 'token' => $request->get('token', null)]);
    }

    public function actionCreate($token)
    {
        $request = Yii::$app->request;
        $survey = new Survey();
        $uploadModel = new SurveyUploadForm();

        $session = Yii::$app->session;
        if (!$session->isActive) $session->open();

        $clientToken = ClientToken::find()->where(['token' => $token])->one();
        if ($token == null || $clientToken == null)
            throw new NotFoundHttpException('Unknown token.');

        $depId = $clientToken->dep_id;

//        if ($session->get('surveyId') == $surveyIdKey) {
//            return $this->redirect(['done']);
//        }

        //странным образом вызов actionCreate происходит 2 раза, поэтому такой костыль
        $sessionKey = $clientToken->store_id . $clientToken->dep_id . $session->getId();
        if ($sessionModel = Session::find()->where(['key' => $sessionKey])->one()) {
            //todo 1 расскоментировать на локальном (у себя, почему-то вызывается 2 раза action)
//            if ($sessionModel->step != 1) {
//                return $this->redirect(['done', 'token' => $token]);
//            }
            return $this->redirect(['done']);
        }

        //save survey
        if ($request->isPost) {
            if ($survey->load($request->post()) && $survey->validate()) {
//                if (in_array($survey->nps, Survey::BAD_NPS)) {
//                    $survey->status = Survey::STATUS_NEW;
//                }

                //странным образом вызов actionCreate происходит 2 раза, поэтому такой костыль
                $sessionKey = $clientToken->store_id . $clientToken->dep_id . $session->getId();
                if ($sessionModel = Session::find()->where(['key' => $sessionKey])->one()) {
                    if ($sessionModel->step == 1) {
                        $sessionModel->step = 2;
                        $sessionModel->save(false);
                        return $this->redirect(['contact', 'survey_id' => $sessionModel->survey_id, 'token' => $token]);
                    } else {
                        return $this->redirect(['done']);
                    }
                }

                $survey->store_id = $clientToken->store_id;
                $survey->dep_id = $clientToken->dep_id;
                $survey->survey_type_id = $clientToken->survey_type_id;
                $survey->nps = $survey->question1; //todo 1 переделать логику заполнения nps
                //todo 1 проверить обнуление id в удаленном phpadmin
                $survey->status = Survey::STATUS_NEW;
                if (!empty($survey->comment)) {
                    $survey->comment = Html::encode($survey->comment);
                }
                $survey->save(false);

                $survey->imageFiles = UploadedFile::getInstances($survey, 'imageFiles');
//                $response = $survey->upload();

//                $session->set('surveyId', $surveyIdKey);
//                $session->close();

                if ($survey->upload()) {
                    $sessionModel = new Session();
                    $sessionModel->key = $sessionKey;
                    $sessionModel->survey_id = $survey->survey_id;
                    $sessionModel->save(false);

                    //redirect to contact
                    return $this->redirect(['contact', 'survey_id' => $sessionModel->survey_id, 'token' => $token]);
                }
            }
        }

        return $this->render('create' . ucfirst(SurveyType::$values[$depId]), [
            'survey' => $survey,
        ]);

    }

    public function actionContact()
    {
        $request = Yii::$app->request;
        $get = $request->get();
        $client = new Client();

        $session = Yii::$app->session;
        if (!$session->isActive) $session->open();

        if ($get['token']) {
            $clientToken = ClientToken::find()->where(['token' => $get['token']])->one();
            if ($get['token'] == null || $clientToken == null)
                throw new NotFoundHttpException('Unknown token.');
            $sessionKey = $clientToken->store_id . $clientToken->dep_id . $session->getId();

            if ($sessionModel2 = Session::find()->where(['key' => $sessionKey, 'contact' => 1])->one()) {
                //todo 1 расскоментировать на локальном (у себя, почему-то вызывается 2 раза action)
//                if ($sessionModel2->step != 1) {
//                    return $this->redirect(['done']);
//                }
                return $this->redirect(['done']);
            }

            $sessionModel = Session::find()->where(['key' => $sessionKey])->one();
        }

        if ($request->isPost) {
            if ($client->load($request->post()) && $client->validate()) {
                $phone = $client['phoneCode'] . $client->phone;
                if (Client::find()->where(['phone' => $phone])->exists() == false) {
                    $client->phone = $phone;
                    $client->save(false);
                }

                $survey = Survey::findOne($client['surveyId']);
                $survey->client_id = $client['surveyId'];
                $survey->save(false);

                $sessionModel->contact = 1;
                $sessionModel->save(false);

                return $this->redirect(['thanks']);
            }
        }

        return $this->render('contact', [
            'client' => $client,
            'surveyId' => Yii::$app->request->get('survey_id') ?? '',
        ]);
    }

    public function actionThanks()
    {
        return $this->render('thanks', [
        ]);
    }

    public function actionDone()
    {
        return $this->render('done');
    }

}
