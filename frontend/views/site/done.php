<?php

use yii\helpers\Html;

?>

<div class="page-wrapper">
    <div class="inner-content">
        <div class="logo">
            <?= Html::img('@web/img/logo.svg', ['alt' => 'magnit-opros']) ?>
        </div>
        <div class="thanks-block">
            <div class="header">
                Вы уже <br>
                проходили <br>
                опрос!
            </div>
            <div class="text">
                Ваше мнение очень важно для нас
            </div>
        </div>
    </div>
</div>