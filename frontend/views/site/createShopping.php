<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

?>

<div class="page-wrapper">
    <div class="inner-content">
        <div class="logo">
            <?= Html::img('@web/img/logo.svg', ['alt' => 'magnit-opros']) ?>
        </div>
        <div class="pool-header">
            Покупательские шкафчики (Общее впечатление о магазине)
        </div>
        <div class="form-wrap">
            <?php $form = ActiveForm::begin([
                'id' => 'survey-form',
                'options' => [
                    'class' => 'form-valid',
                    'enctype' => 'multipart/form-data',
                ],
            ]); ?>
            <div class="form-group">
                <div class="question">
                    <div class="number">
                        <div class="current">1/</div>
                        <div class="all">4</div>
                    </div>
                    <div class="name">
                        Оцените дружелюбность и отзывчивость персонала
                    </div>
                </div>
                <div class="input-wrap">
                    <div class="radio-wrapper radio-icons">
                        <input type="radio" id="q-1-5" name="Survey[question1]" value="5" data-reqired/>
                        <label for="q-1-5" title="text"></label>

                        <input type="radio" id="q-1-4" name="Survey[question1]" value="4" data-reqired/>
                        <label for="q-1-4" title="text"></label>

                        <input type="radio" id="q-1-3" name="Survey[question1]" value="3" data-reqired/>
                        <label for="q-1-3" title="text"></label>

                        <input type="radio" id="q-1-2" name="Survey[question1]" value="2" data-reqired/>
                        <label for="q-1-2" title="text"></label>

                        <input type="radio" id="q-1-1" name="Survey[question1]" value="1" data-reqired/>
                        <label for="q-1-1" title="text"></label>
                    </div>
                    <div class="label-wrap">
                        <div class="label">Плохо</div>
                        <div class="label">Отлично</div>
                    </div>
                    <div class="error-text">
                        Выберите ответ
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="question">
                    <div class="number">
                        <div class="current">2/</div>
                        <div class="all">4</div>
                    </div>
                    <div class="name">
                        Оцените ассортимент в магазине
                    </div>
                </div>
                <div class="input-wrap">
                    <div class="radio-wrapper radio-icons">
                        <input type="radio" id="q-2-5" name="Survey[question2]" value="5" data-reqired/>
                        <label for="q-2-5" title="text"></label>

                        <input type="radio" id="q-2-4" name="Survey[question2]" value="4" data-reqired/>
                        <label for="q-2-4" title="text"></label>

                        <input type="radio" id="q-2-3" name="Survey[question2]" value="3" data-reqired/>
                        <label for="q-2-3" title="text"></label>

                        <input type="radio" id="q-2-2" name="Survey[question2]" value="2" data-reqired/>
                        <label for="q-2-2" title="text"></label>

                        <input type="radio" id="q-2-1" name="Survey[question2]" value="1" data-reqired/>
                        <label for="q-2-1" title="text"></label>
                    </div>
                    <div class="label-wrap">
                        <div class="label">Плохо</div>
                        <div class="label">Отлично</div>
                    </div>
                    <div class="error-text">
                        Выберите ответ
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="question">
                    <div class="number">
                        <div class="current">3/</div>
                        <div class="all">4</div>
                    </div>
                    <div class="name">
                        Насколько Вы готовы рекомендовать данный магазин?
                    </div>
                </div>
                <div class="input-wrap">
                    <div class="radio-wrapper radio-icons">
                        <input type="radio" id="q-3-5" name="Survey[question3]" value="5" data-reqired/>
                        <label for="q-3-5" title="text"></label>

                        <input type="radio" id="q-3-4" name="Survey[question3]" value="4" data-reqired/>
                        <label for="q-3-4" title="text"></label>

                        <input type="radio" id="q-3-3" name="Survey[question3]" value="3" data-reqired/>
                        <label for="q-3-3" title="text"></label>

                        <input type="radio" id="q-3-2" name="Survey[question3]" value="2" data-reqired/>
                        <label for="q-3-2" title="text"></label>

                        <input type="radio" id="q-3-1" name="Survey[question3]" value="1" data-reqired/>
                        <label for="q-3-1" title="text"></label>
                    </div>
                    <div class="label-wrap">
                        <div class="label">Совсем не готов</div>
                        <div class="label">Готов</div>
                    </div>
                    <div class="error-text">
                        Выберите ответ
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="question">
                    <div class="number">
                        <div class="current">4/</div>
                        <div class="all">4</div>
                    </div>
                    <div class="name">
                        Если у Вас есть предложения, просьба оставить их здесь
                    </div>
                </div>
                <div class="input-wrap">
                    <textarea name="Survey[comment]"></textarea>
                    <div class="attach-file">
                        <div class="files-container">
                        </div>
                        <div class="file-input">
                            <input type="file" name="Survey[imageFiles][]" id="q-5" multiple class="input-file"
                                   accept="image/*" aria-invalid="true">
                            <label for="q-5"></label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="btn-wrap">
                <button class="btn-submit">Отправить</button>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>