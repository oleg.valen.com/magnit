<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

?>

<div class="page-wrapper">
    <div class="inner-content">
        <div class="logo">
            <?= Html::img('@web/img/logo.svg', ['alt' => 'magnit-opros']) ?>
        </div>
        <div class="pool-header">
            Сервисная стойка
        </div>
        <div class="form-wrap">
            <?php $form = ActiveForm::begin([
                'id' => 'survey-form',
                'options' => [
                    'class' => 'form-valid',
                    'enctype' => 'multipart/form-data',
                ],
            ]); ?>
            <div class="form-group">
                <div class="question">
                    <div class="number">
                        <div class="current">1/</div>
                        <div class="all">3</div>
                    </div>
                    <div class="name">
                        Решили ли сотрудники Ваш вопрос?
                    </div>
                </div>
                <div class="input-wrap">
                    <div class="radio-wrapper radio-label">
                        <div class="radio-item">
                            <input type="radio" id="q-1-1" name="Survey[question1]" value="1" data-reqired/>
                            <label for="q-1-1" title="text">Да</label>
                        </div>
                        <div class="radio-item">
                            <input type="radio" id="q-1-2" name="Survey[question1]" value="2" data-reqired/>
                            <label for="q-1-2" title="text">Нет</label>
                        </div>
                    </div>
                    <div class="error-text">
                        Выберите ответ
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="question">
                    <div class="number">
                        <div class="current">2/</div>
                        <div class="all">3</div>
                    </div>
                    <div class="name">
                        Стремились ли сотрудники помочь Вам?
                    </div>
                </div>
                <div class="input-wrap">
                    <div class="radio-wrapper radio-label">
                        <div class="radio-item">
                            <input type="radio" id="q-2-1" name="Survey[question2]" value="1" data-reqired/>
                            <label for="q-2-1" title="text">Да</label>
                        </div>
                        <div class="radio-item">
                            <input type="radio" id="q-2-2" name="Survey[question2]" value="2" data-reqired/>
                            <label for="q-2-2" title="text">Нет</label>
                        </div>
                    </div>
                    <div class="error-text">
                        Выберите ответ
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="question">
                    <div class="number">
                        <div class="current">3/</div>
                        <div class="all">3</div>
                    </div>
                    <div class="name">
                        Если у Вас есть предложения, просьба оставить их здесь
                    </div>
                </div>
                <div class="input-wrap">
                    <textarea name="Survey[comment]"></textarea>
                    <div class="attach-file">
                        <div class="files-container">
                        </div>
                        <div class="file-input">
                            <input type="file" name="Survey[imageFiles][]" id="q-4" multiple class="input-file"
                                   accept="image/*" aria-invalid="true">
                            <label for="q-4"></label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="btn-wrap">
                <button class="btn-submit">Отправить</button>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>