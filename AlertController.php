<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;


Class AlertController extends Controller
{
    public function __construct()
    {

        return Yii::$app->mailer
            ->compose()
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
            ->setTo('budonnyi@gmail.com')
            ->setSubject('CRON appointment ' . Yii::$app->name)
            ->send();

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://shopperbonus4service.com/admin/site/get-api-data",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        exit();
    }
}

$some = new AlertController();

die;
