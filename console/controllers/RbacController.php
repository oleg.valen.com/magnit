<?php

namespace console\controllers;

use common\rbac\AuthorRule;
use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        //permissions
        $viewGM = $auth->createPermission('viewGM');
        $viewGM->description = 'View GM';
        $auth->add($viewGM);

        $viewBF = $auth->createPermission('viewBF');
        $viewBF->description = 'View BF';
        $auth->add($viewBF);

        $viewRMKS = $auth->createPermission('viewRMKS');
        $viewRMKS->description = 'View RMKS';
        $auth->add($viewRMKS);

        $viewOperBF = $auth->createPermission('viewOperBF');
        $viewOperBF->description = 'View oper BF';
        $auth->add($viewOperBF);

        $viewSpecOffice = $auth->createPermission('viewSpecOffice');
        $viewSpecOffice->description = 'View spec office';
        $auth->add($viewSpecOffice);

        $viewSpecManag = $auth->createPermission('viewSpecManag');
        $viewSpecManag->description = 'View spec manag';
        $auth->add($viewSpecManag);

        $viewSpecHead = $auth->createPermission('viewSpecHead');
        $viewSpecHead->description = 'View spec head';
        $auth->add($viewSpecHead);

        //roles

        $GM = $auth->createRole('GM');
        $auth->add($GM);
        $auth->addChild($GM, $viewGM);

        $BF = $auth->createRole('BF');
        $auth->add($BF);
        $auth->addChild($BF, $viewBF);

        $RMKS = $auth->createRole('RMKS');
        $auth->add($RMKS);
        $auth->addChild($RMKS, $viewRMKS);

        $operBF = $auth->createRole('operBF');
        $auth->add($operBF);
        $auth->addChild($operBF, $viewOperBF);

        $specOffice = $auth->createRole('specOffice');
        $auth->add($specOffice);
        $auth->addChild($specOffice, $viewSpecOffice);

        $specManag = $auth->createRole('specManag');
        $auth->add($specManag);
        $auth->addChild($specManag, $viewSpecManag);

        $specHead = $auth->createRole('specHead');
        $auth->add($specHead);
        $auth->addChild($specHead, $viewSpecHead);

        $admin = $auth->createRole('admin');
        $auth->add($admin);
        $auth->addChild($admin, $GM);
        $auth->addChild($admin, $BF);
        $auth->addChild($admin, $RMKS);
        $auth->addChild($admin, $operBF);
        $auth->addChild($admin, $specOffice);
        $auth->addChild($admin, $specManag);
        $auth->addChild($admin, $specHead);

        //assigns
        $auth->assign($admin, 1);

        $auth->assign($GM, 9);

        $auth->assign($BF, 10);

        $auth->assign($RMKS, 11);

    }
}