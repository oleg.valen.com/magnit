<?php

namespace console\controllers;

use console\services\CronService;
use frontend\models\ContactForm;
use Yii;
use yii\console\Controller;
use yii\helpers\Json;

class CronController extends Controller
{

    private $cron;

    public function init()
    {
        parent::init();
        $this->cron = new CronService();
    }

    public function afterAction($action, $result)
    {
        $result = parent::afterAction($action, $result);

        if (CronService::$errors) {
            $form = new ContactForm([
                'email' => Yii::$app->params['receiverEmail'],
                'name' => Yii::$app->params['receiverName'],
                'subject' => 'Inventive api error ' . date('H:i:s d-m-Y'),
                'body' => Json::encode(CronService::$errors, JSON_PRETTY_PRINT),
            ]);
            try {
                $form->sendEmail(Yii::$app->params['receiverEmail']);
            } catch (\Exception $e) {
                Yii::error([
                    'name' => 'Start: ' . __METHOD__,
                    'time' => $start = microtime(true),
                    'error' => $e,
                ], 'api');
            }
        }

        return $result;
    }

    public function actionManageNotifications()
    {
        return;
        Yii::info([
            'name' => 'Start: ' . __METHOD__,
            'time' => $start = microtime(true),
        ], 'api');

        try {
            $this->cron->manageNotifications();
        } catch (\Exception $e) {
            $this->cron::$errors[] = $e->getMessage();
            Yii::error([
                'name' => 'api',
                'error' => $e->getMessage()], 'api');
        }

        Yii::info([
            'name' => 'Finish: ' . __METHOD__,
            'time' => microtime(true) - $start,
            'memory' => memory_get_usage(true),
            'emailSentTotal' => CronService::$emailSentTotal,
        ], 'api');
    }

}
