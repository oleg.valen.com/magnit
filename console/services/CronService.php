<?php

namespace console\services;

use common\models\Survey;
use common\models\DeliverySurveyStatus\DeliverySurveyContext;
use common\models\SurveyStatus\SurveyContext;
use Yii;

class CronService
{

    public static $errors = [];
    public static $emailSentTotal = 0;

    public function manageNotifications()
    {
//        $deliverySurveys = Survey::find()
//            ->where(['status' => Survey::STATUS_INIT])
//            ->andWhere(['in', 'nps', Survey::BAD_NPS])
//            ->andWhere(['not', ['nps' => null]])
//            ->all();
//        foreach ($deliverySurveys as $item) {
//            $item->status = Survey::STATUS_NEW;
//            $item->save(false);
//        }

        $surveys = Survey::find()
            ->where(['in', 'nps', Survey::BAD_NPS])
            ->andWhere(['in', 'status', [
                Survey::STATUS_NEW,
//                Survey::STATUS_CLOSED_FINAL,
//                Survey::STATUS_CONFIRMATION,
//                Survey::STATUS_NOT_CLOSED_FINAL,
            ]])
//            ->andWhere(['>=', 'created_at', strtotime('2021-11-23')])
            ->all();

        foreach ($surveys as $item) {
            $class = '\common\models\SurveyStatus\\' . Survey::STATUS_CLASSES[$item->status];
            $surveyContext = new SurveyContext($item, new $class(['statusUpdatedAt' => $item->status_updated_at]));
            $surveyContext->handle();
//            return; //test
        }
    }

}
