<?php

namespace backend\controllers;

use backend\models\SurveyUploadForm;
use common\models\Area;
use common\models\City;
use common\models\Client;
use common\models\DeliveryDoc;
use common\models\Department;
use common\models\SearchForm;
use common\models\Survey;
use common\models\DeliverySurveySearch;
use common\models\ClientToken;
use common\models\DeliverySurveyStatus\DeliverySurveyContext;
use common\models\Factory;
use common\models\Log;
use common\models\StatusDelivery;
use common\models\SurveyStatus\SurveyContext;
use common\models\SurveyType;
use common\models\TransportCompany;
use common\models\User;
use common\services\SmsService;
use console\controllers\CronController;
use console\services\CronService;
use yii\filters\AccessControl;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use Yii;
use yii\web\UploadedFile;
use yii\data\Pagination;

/**
 * DeliverySurveyController implements the CRUD actions for DeliverySurvey model.
 */
class SurveyController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'thanks', 'done', 'upload', 'update'],
                'rules' => [
                    [
                        'actions' => ['create', 'thanks', 'done'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index', 'upload', 'update'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all DeliverySurvey models.
     * @return mixed
     */
    public function actionIndex()
    {
        $request = Yii::$app->request;
        $post = $request->post();
        $form = null;
//        $uploadModel = new SurveyUploadForm();
        $die = [
            'totalAll' => 0,
            'total' => 0,
            'badNps' => 0,
            'expired' => 0,
            'new' => 0,
            'taken' => 0,
        ];
        $session = Yii::$app->session;
        $user = Yii::$app->user->getIdentity();
        $searchModel = new SearchForm();

        $query = Survey::find()
            ->joinWith('store')
            ->where(['not', ['nps' => null]]);

        if ($user->store_id)
            $query = $query->andWhere(['store_id' => $user->store_id]);
        if ($user->area_id)
            $query = $query->andWhere(['store.area_id' => $user->area_id]);

        if ($request->isPost) {
            if (isset($post['Form'])) {
                $form = $post['Form'];
                list($date0, $date1) = $this->parseDate($form['createdAt']);
                $query = $query->andWhere([
                    'between',
                    'created_at',
                    strtotime($date0), strtotime('tomorrow', strtotime($date1)) - 1
                ]);
                //todo переделать условия, фильтры по post, session
                if ($form['storeId']) {
                    $query = $query->andWhere(['store_id' => $form['storeId']]);
                    $session->set('storeId', $form['storeId']);
                }
                if ($form['departmentId']) {
                    $query = $query->andWhere(['dep_id' => $form['departmentId']]);
                    $session->set('departmentId', $form['departmentId']);
                }
                if ($form['surveyTypeId']) {
                    $query = $query->andWhere(['survey_type_id' => $form['surveyTypeId']]);
                    $session->set('surveyTypeId', $form['surveyTypeId']);
                }
                if (isset($post['mode'])) {
                    if ($post['mode'] == 'all') {
                        //affects only on numbers in dies (not refresh page)
                    } elseif ($post['mode'] == 'bad-nps') {
                        $query = $query->andWhere(['in', 'nps', Survey::BAD_NPS]);
                    } elseif ($post['mode'] == 'expired') {
                        $query = $query->andWhere(['status' => Survey::STATUS_GM_EXPIRED]);
                    } elseif ($post['mode'] == 'new') {
                        $query = $query->andWhere(['status' => Survey::STATUS_NEW]);
                    } elseif ($post['mode'] == 'taken') {
                        $query = $query->andWhere(['status' => Survey::STATUS_GM_TAKEN]);
                    }
                }
                $session->set('date0', $date0);
                $session->set('date1', $date1);
            } elseif (isset($post['SearchForm'])) {
                $form = $post['SearchForm'];
                list($date0, $date1) = $this->parseDate($form['createdAt']);

                $search = Html::decode($form['search']);
                $query = $query->andFilterWhere(['or',
                    ['=', 'survey_id', $search],
                    ['like', 'comment', $search]]);
            }
        } else {
            if ($session->has('storeId'))
                $query = $query->andWhere(['store_id' => $session->get('storeId')]);
            if ($session->has('departmentId'))
                $query = $query->andWhere(['dep_id' => $session->get('departmentId')]);
            if ($session->has('surveyTypeId'))
                $query = $query->andWhere(['survey_type_id' => $session->get('surveyTypeId')]);

            $date0 = $session->get('date0', date('01-m-Y'));
            $date1 = $session->get('date1', date('d-m-Y', time()));
            $query = $query->andWhere([
                'between',
                'created_at',
                strtotime($date0), strtotime('tomorrow', strtotime($date1)) - 1
            ]);
        }

        $surveysTotal = Survey::find()
            ->joinWith('store')
            ->where([
                'between',
                'created_at',
                strtotime($date0), strtotime('tomorrow', strtotime($date1)) - 1
            ]);
        if ($user->store_id)
            $surveysTotal = $surveysTotal->andWhere(['store_id' => $user->store_id]);
        if ($user->area_id)
            $surveysTotal = $surveysTotal->andWhere(['store.area_id' => $user->area_id]);
        $die['totalAll'] = $surveysTotal->count();

        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);
        $surveys = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->orderBy(['survey_id' => SORT_DESC])
            ->all();

        foreach ($countQuery->all() as $item) {
            $die['total']++;
            $die['badNps'] += in_array($item->nps, Survey::BAD_NPS, true) ? 1 : 0;
            $die['expired'] += $item->status == Survey::STATUS_GM_EXPIRED ? 1 : 0;
            $die['new'] += $item->status == Survey::STATUS_NEW ? 1 : 0;
            $die['taken'] += $item->status == Survey::STATUS_GM_TAKEN ? 1 : 0;
        }
        $die['badNps'] = round($die['total'] !== 0 ? ($die['badNps'] / $die['total'] * 100) : 0, 1);

        return $this->render('index', [
            'id' => Yii::$app->controller->id,
            'surveys' => $surveys,
//            'uploadModel' => $uploadModel,
            'pages' => $pages,
            'filters' => [
                'createdAt' => $date0 . ' - ' . $date1,
                'storeId' => isset($form['storeId']) ? $form['storeId'] : ($session->has('storeId') ? $session->get('storeId') : ''),
                'departmentId' => isset($form['departmentId']) ? $form['departmentId'] : ($session->has('departmentId') ? $session->get('departmentId') : ''),
                'surveyTypeId' => isset($form['surveyTypeId']) ? $form['surveyTypeId'] : ($session->has('surveyTypeId') ? $session->get('surveyTypeId') : ''),
                'die' => $die,
            ],
            'searchModel' => $searchModel,
        ]);
    }

    public function actionDeliverySurveyService()
    {

        $deliverySurveys = Survey::find()
            ->where(['status' => Survey::STATUS_INIT])
            ->andWhere(['in', 'nps', Survey::BAD_NPS])
            ->andWhere(['not', ['nps' => null]])
            ->all();
        foreach ($deliverySurveys as $item) {
            $item->status = Survey::STATUS_NEW;
            $item->save(false);
        }

//        $user = Yii::$app->user->getIdentity();
//        return in_array($user->id, Yii::$app->authManager->getUserIdsByRole('admin'))
//            || in_array($user->access, User::$levels[1]);

        //        Yii::info([
//            'name' => 'Start: ' . __METHOD__,
//            'time' => $start = microtime(true),
//        ], 'api');
//
//        try {
//            $cron = new CronService();
//            $cron->manageNotifications();
//        } catch (\Exception $e) {
//            $cron::$errors[] = $e->getMessage();
//            Yii::error([
//                'name' => 'api',
//                'error' => $e->getMessage()], 'api');
//        }
//
//        Yii::info([
//            'name' => 'Finish: ' . __METHOD__,
//            'time' => microtime(true) - $start,
//            'memory' => memory_get_usage(true),
//            'emailSentTotal' => CronService::$emailSentTotal,
//        ], 'api');

    }

    /**
     * Displays a single DeliverySurvey model.
     * @param int $survey_id Номер анкеты
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($survey_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($survey_id),
        ]);
    }

    /**
     * Creates a new DeliverySurvey model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($token)
    {
        $request = Yii::$app->request;
        $survey = new Survey();

        $session = Yii::$app->session;
        if (!$session->isActive) $session->open();

        $clientToken = ClientToken::find()->where(['token' => $token])->one();
        if ($token == null || $clientToken == null)
            throw new NotFoundHttpException('Unknown token.');

        $surveyIdKey = $clientToken->store_id . $clientToken->dep_id . $session->getId();
        if ($session->get('surveyId') == $surveyIdKey) {
            return $this->redirect(['done']);
        }

        //save survey
        if ($request->isPost) {
            if ($survey->load($request->post()) && $survey->validate()) {
//                if (in_array($survey->nps, Survey::BAD_NPS)) {
//                    $survey->status = Survey::STATUS_NEW;
//                }

                $survey->store_id = $clientToken->store_id;
                $survey->dep_id = $clientToken->dep_id;
                $survey->survey_type_id = $clientToken->survey_type_id;
                $survey->status = Survey::STATUS_NEW;
                if (!empty($survey->comment)) {
                    $survey->comment = Html::encode($survey->comment);
                }
                $survey->save(false);

                $session->set('surveyId', $surveyIdKey);
                $session->close();

                //redirect to contact
                return $this->redirect(['contact', 'survey_id' => $survey->survey_id]);
            }
        }

        return $this->render('create' . ucfirst(SurveyType::$values[$clientToken->dep_id]), [
        ]);

        //сохранение контактов

        //редирект на страницу done


        $deliverySurvey = Survey::find()->where(['doc_id' => $clientToken->doc_id])->one();
        if ($deliverySurvey->nps != null)
            $this->redirect(['done']);

        if ($request->isPost) {
            if ($deliverySurvey->load($request->post()) && $deliverySurvey->validate()) {

                if (in_array($deliverySurvey->nps, Survey::BAD_NPS)) {
                    $deliverySurvey->status = Survey::STATUS_NEW;
                }
                if (!empty($deliverySurvey->comment)) {
                    $deliverySurvey->comment = Html::encode($deliverySurvey->comment);
                }
                $deliverySurvey->save(false);
                $this->redirect(['thanks', 'username' => $deliverySurvey->client->name]);
            }
        }

        return $this->render('create', [
            'deliverySurvey' => $deliverySurvey,
            'values' => Survey::$values,
        ]);
    }

    /**
     * Updates an existing DeliverySurvey model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $survey_id Номер анкеты
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($survey_id)
    {
        $model = $this->findModel($survey_id);
        $request = Yii::$app->request;
        $images = [];

        if ($request->isAjax) {
            if ($request->post('mode') == 'taken') {
                $model->status = Survey::STATUS_GM_TAKEN;
                $model->save(false);
                $log = new Log([
                    'survey_id' => $survey_id,
                    'user_id' => Yii::$app->user->id,
                    'comment' => !empty($request->post('revision-comment')) ? Html::encode($request->post('revision-comment')) : null,
                    'status' => Survey::STATUS_GM_TAKEN,
                ]);
                $log->save(false);
            } elseif ($request->post('mode') == 'accepted') {
                $model->status = Survey::STATUS_BF_CLOSED;
                $model->save(false);

                $log = new Log([
                    'survey_id' => $survey_id,
                    'user_id' => Yii::$app->user->id,
                    'comment' => !empty($request->post('revision-comment')) ? Html::encode($request->post('revision-comment')) : null,
                    'status' => Survey::STATUS_BF_CLOSED,
                ]);
                $log->save(false);

                //todo сделать метод, чтобы уйти от повторяющегося кода - 3 строки
                //todo 1 что-то делаем в связи с изменением статуса..
//                $class = '\common\models\SurveyStatus\\' . Survey::STATUS_CLASSES[$model->status];
//                $surveyContext = new SurveyContext($model, new $class(['statusUpdatedAt' => $model->status_updated_at]));
//                $surveyContext->handle();
            } elseif ($request->post('mode') == 'revision') {
                $model->status = Survey::STATUS_BF_REVISION;
                $model->save(false);

                $log = new Log([
                    'survey_id' => $survey_id,
                    'user_id' => Yii::$app->user->id,
                    'comment' => !empty($request->post('revision-comment')) ? Html::encode($request->post('revision-comment')) : null,
                    'status' => Survey::STATUS_BF_REVISION,
                ]);
                $log->save(false);

//                $class = '\common\models\SurveyStatus\\' . Survey::STATUS_CLASSES[$model->status];
//                $surveyContext = new SurveyContext($model, new $class(['statusUpdatedAt' => $model->status_updated_at]));
//                $surveyContext->handle();
            }
            return;
        }

        if ($this->request->isPost) {
            if ($request->post('mode') == 'taken') {
                $model->status = Survey::STATUS_GM_TAKEN;
                $model->save(false);
                $log = new Log([
                    'survey_id' => $survey_id,
                    'user_id' => Yii::$app->user->id,
                    'comment' => !empty($request->post('comment')) ? Html::encode($request->post('comment')) : null,
                    'status' => Survey::STATUS_GM_TAKEN,
                ]);
                $log->save(false);
            } elseif ($request->post('mode') == 'closed') {
                $model->status = Survey::STATUS_GM_CLOSED;
                $model->save(false);
                $log = new Log([
                    'survey_id' => $survey_id,
                    'user_id' => Yii::$app->user->id,
                    'comment' => !empty($request->post('comment')) ? Html::encode($request->post('comment')) : null,
                    'status' => Survey::STATUS_GM_CLOSED,
                ]);
                $log->save(false);

//            $class = '\common\models\SurveyStatus\\' . Survey::STATUS_CLASSES[$model->status];
//            $surveyContext = new SurveyContext($model, new $class(['statusUpdatedAt' => $model->status_updated_at]));
//            $surveyContext->handle();
            }

            return $this->redirect(['update', 'survey_id' => $survey_id]);
        }

        foreach (glob(Yii::getAlias('@frontend') . '/web/uploads/' . $model->survey_id . '_*') as $item) {
            $images[] = pathinfo($item)['basename'];
        }

        return $this->render('update', [
            'id' => Yii::$app->controller->id,
            'model' => $model,
            'logs' => $model->logs,
            'images' => $images,
        ]);
    }

    /**
     * Deletes an existing DeliverySurvey model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $survey_id Номер анкеты
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($survey_id)
    {
        $this->findModel($survey_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the DeliverySurvey model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $survey_id Номер анкеты
     * @return Survey the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($survey_id)
    {
        if (($model = Survey::findOne($survey_id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist . ');
    }

    public function actionContact()
    {
        $request = Yii::$app->request;
        $client = new Client();

        if ($request->isPost) {
            if ($client->load($request->post()) && $client->validate()) {
                $phone = $client['phoneCode'] . $client->phone;
                if (Client::find()->where(['phone' => $phone])->exists() == false) {
                    $client->phone = $phone;
                    $client->save(false);
                }

                $survey = Survey::findOne($client['surveyId']);
                $survey->client_id = $client->client_id;
                $survey->save(false);

                return $this->redirect(['thanks']);
            }
        }

        return $this->render('contact', [
            'client' => $client,
            'surveyId' => Yii::$app->request->get('survey_id') ?? '',
        ]);
    }

    public function actionThanks()
    {
        return $this->render('thanks', [
        ]);
    }

    public function actionDone()
    {
        return $this->render('done');
    }

    public function actionUpload()
    {
        ini_set("memory_limit", "2048M");
        ini_set('max_execution_time', 2400);

        $areas = Area::find()->indexBy('name')->all();
        $cities = City::find()->indexBy('name')->all();
        $factories = Factory::find()->indexBy('name')->all();
        $transportCompanies = TransportCompany::find()->indexBy('name')->all();
        $statusDeliveries = StatusDelivery::find()->indexBy('name')->all();

        $uploadModel = new SurveyUploadForm();
        if (Yii::$app->request->isPost) {
            $uploadModel->xlsxFile = UploadedFile::getInstance($uploadModel, 'xlsxFile');
            if ($path = $uploadModel->upload()) {
                if ($xlsx = \SimpleXLSX::parse($path)) {
                    $items = $xlsx->rows();
                } else {
                    echo SimpleXLSX::parseError();
                }
                foreach ($items as $key => $item) {
                    try {
                        if ($key == 0) continue;

                        if (array_key_exists($item[11], $areas)) {
                            $area = $areas[$item[11]];
                        } else {
                            $area = new Area(['name' => $item[11]]);
                            $area->save(false);
                            $areas[$area->name] = $area;
                        }

                        if (array_key_exists($item[12], $cities)) {
                            $city = $cities[$item[12]];
                        } else {
                            $city = new City(['name' => $item[12]]);
                            $city->save(false);
                            $cities[$city->name] = $city;
                        }

                        if (array_key_exists($item[9], $factories)) {
                            $factory = $factories[$item[9]];
                        } else {
                            $factory = new Factory(['name' => $item[9]]);
                            $factory->save(false);
                            $factories[$factory->name] = $factory;
                        }

                        if (array_key_exists($item[7], $transportCompanies)) {
                            $transportCompany = $transportCompanies[$item[7]];
                        } else {
                            $transportCompany = new TransportCompany(['name' => $item[7]]);
                            $transportCompany->save(false);
                            $transportCompanies[$transportCompany->name] = $transportCompany;
                        }

                        if (array_key_exists($item[6], $statusDeliveries)) {
                            $statusDelivery = $statusDeliveries[$item[6]];
                        } else {
                            $statusDelivery = new StatusDelivery(['name' => $item[6]]);
                            $statusDelivery->save(false);
                            $statusDeliveries[$statusDelivery->name] = $statusDelivery;
                        }

                        if (!$client = Client::find()->where(['phone' => $item[13]])->one()) {
                            $client = new Client(['phone' => $item[13]]);
                        }
                        $client->name = $item[15];
                        $client->surname = $item[16];
                        $client->email = $item[14];
                        $client->save(false);

                        $deliveryDoc = DeliveryDoc::find()
                            ->where(['numdoc_1c' => $item[3]])
                            ->orderBy(['doc_id' => SORT_DESC])
                            ->one();
                        if ($deliveryDoc != null && (date('Y.m.d', $deliveryDoc->deliverySurvey->created_at) == date('Y.m.d', time()))) {

                        } else {
                            $deliveryDoc = new DeliveryDoc(['numdoc_1c' => $item[3]]);
                        }

                        $deliveryDoc->client_id = $client->client_id;
                        $deliveryDoc->numdoc_crm = $item[2];
                        $deliveryDoc->sum = $item[4];
                        $deliveryDoc->delivery_date = strtotime($item[5]);
                        $deliveryDoc->transport_company_id = $transportCompany->id;
                        $deliveryDoc->status_delivery_id = $statusDelivery->id;
                        $deliveryDoc->time_range = $item[8];
                        $deliveryDoc->factory_id = $factory->id;
                        $deliveryDoc->area_id = $area->id;
                        $deliveryDoc->city_id = $city->id;
                        $deliveryDoc->save(false);

                        unset($item);

                    } catch (\Exception $e) {
//                        self::$errors[] = $e->getMessage();
//                        Yii::error([
//                            'name' => 'bonuseCalculation',
//                            'data' => ['date' => $date],
//                            'error' => $e->getMessage(),
//                        ], 'api');
                    }
                }
                unset($item);

                $this->actionDeliverySurveyCreate();

                @unlink($path);
            }
        }
        $this->redirect(Yii::$app->request->referrer);
    }

    public function actionDeliverySurveyCreate()
    {
        $deliveryDocs = DeliveryDoc::find()
            ->leftJoin('delivery_survey', '`delivery_doc`.`doc_id` = `delivery_survey`.`doc_id`')
            ->where(['is', 'delivery_survey.survey_id', null])
            ->all();
        foreach ($deliveryDocs as $item) {
            if ($item->deliverySurvey == null) {
                $deliverySurvey = new Survey();
                $deliverySurvey->client_id = $item->client_id;
                $deliverySurvey->doc_id = $item->doc_id;
                $deliverySurvey->save(false);
            }
        }
    }

    public function actionSendSms()
    {
        $deliverySurveys = Survey::find()
            ->where(['sms_sent' => null])
            ->all();
        foreach ($deliverySurveys as $item) {
            $token = new ClientToken();
            $token->client_id = $item->client->client_id;
            $token->doc_id = $item->doc_id;
            $token->generateToken();
            $token->save(false);

            $sms = new SmsService(Yii::$app->request->hostInfo . '/delivery/' . $token->token);
            $sms->send($item);
        }
        $this->redirect(Yii::$app->request->referrer);
    }

    public function actionUploadXlsx()
    {

        $request = Yii::$app->request;
        $post = $request->post();
        $form = null;
        $n = 0;

        include_once("xlsxwriter.class.php");
        $filename = "Выгрузка.xlsx";
        header('Content-disposition: attachment; filename="' . \XLSXWriter::sanitize_filename($filename) . '"');
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        $rows = [[
            'N п/п',
            'Статус алерта',
            'Номер',
            'Дата опроса',
            'Оценка',
            'Магазин',
            'Отдел',
            'Опрос',
        ]];

        $query = Survey::find()
            ->where(['not', ['nps' => null]]);

        $form = $post['Form'];
        list($date0, $date1) = $this->parseDate($form['createdAt']);

        $query = $query
            ->andWhere([
                'between',
                'created_at',
                strtotime($date0), strtotime('tomorrow', strtotime($date1)) - 1
            ]);

        if ($form['storeId']) {
            $query = $query->andWhere(['store_id' => $form['storeId']]);
        }
        if ($form['departmentId']) {
            $query = $query->andWhere(['dep_id' => $form['departmentId']]);
        }
        if ($form['surveyTypeId']) {
            $query = $query->andWhere(['survey_type_id' => $form['surveyTypeId']]);
        }
        $query = $query->all();

        foreach ($query as $item) {
            $rows[] = [
                ++$n,
                in_array($item->nps, Survey::BAD_NPS, true) ? 'Новый' : '',
                $item->survey_id,
                date('d.m.Y h:m', $item->created_at),
                $item->question1,
                $item->store->name,
                $item->department->name,
                $item->store->name,
                SurveyType::$valueNames[$item->survey_type_id],
            ];
        }

        $writer = new \XLSXWriter();
        foreach ($rows as $row) {
            $writer->writeSheetRow('Sheet1', $row);
        }
        $writer->writeToStdOut();
        exit(0);
    }

    private function parseDate($str)
    {
        $dates = explode(' - ', $str);
        return [$dates[0], $dates[1]];
    }

}
