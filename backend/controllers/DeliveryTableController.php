<?php

namespace backend\controllers;

use common\models\DeliverySurveySearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use Yii;

/**
 * DeliverySurveyController implements the CRUD actions for DeliverySurvey model.
 */
class DeliveryTableController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index'],
                'rules' => [
//                    [
//                        'actions' => ['create', 'thanks', 'done'],
//                        'allow' => true,
//                    ],
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all DeliverySurvey models.
     * @return mixed
     */
    public function actionIndex()
    {
        $request = Yii::$app->request;
        $post = $request->post();
        $form = null;
        $data = ['items' => []];
        $joins = $wheres = '';

        $startTempl = [
            'total' => 0,
            'filled' => 0,
            'question1' => 0,
            'question2' => 0,
            'question3' => 0,
            'question4' => 0,
            'nps_average' => 0,
            'alert' => 0,
            'status_new' => 0,
            'status_expired' => 0,
            'status_taken' => 0,
            'status_closed' => 0,
        ];

        if ($request->isPost) {
            $form = $post['Form'];
            $dates = explode(' - ', $form['deliveryDate']);
            $date0 = $dates[0];
            $date1 = $dates[1];
            if ($form['transportCompanyId']) {
                $joins .= ' left join transport_company tc on dd.transport_company_id = tc.id ';
                $wheres .= ' AND tc.id =:transport_company_id ';
            }
            if ($form['statusDeliveryId']) {
                $joins .= ' left join status_delivery sd on dd.status_delivery_id = sd.id ';
                $wheres .= ' AND sd.id =:status_delivery_id ';
            }
            if ($form['timeRangeName']) {
                $wheres .= ' AND dd.time_range =: time_range ';
            }
            if ($form['factoryId']) {
                $wheres .= ' AND f.id =:factory_id ';
            }
            if ($form['areaId']) {
//                $joins .= ' left join area a on dd.area_id = a.id ';
                $wheres .= ' AND a.id =:area_id ';
            }
        } else {
            $date0 = date('01-m-Y');
            $date1 = date('d-m-Y', time());
        }

        $sql = "
            select a.name as                                                     f1,
                   c.name   as                                                   f2,
                   f.name as                                                     f3,
                   sum(1)                                                        total,
                   sum(CASE WHEN question1 is not null THEN 1 else 0 end)        filled,
                   COALESCE(sum(question1), 0)                                   question1,
                   COALESCE(sum(question2), 0)                                   question2,
                   COALESCE(sum(question3), 0)                                   question3,
                   COALESCE(sum(question4), 0)                                   question4,
                   sum(nps)                                                      nps_average,
                   sum(CASE WHEN nps in (0, 1, 2, 3, 4, 5, 6) THEN 1 else 0 end) alert,
                   sum(CASE WHEN status = 1 THEN 1 else 0 end)                   status_new,
                   sum(CASE WHEN status = 3 THEN 1 else 0 end)                   status_expired,
                   sum(CASE WHEN status = 2 THEN 1 else 0 end)                   status_taken,
                   sum(CASE WHEN status = 6 THEN 1 else 0 end)                   status_closed
            from delivery_survey ds
                left join delivery_doc dd on dd.doc_id = ds.doc_id
                left join area a on dd.area_id = a.id
                left join city c on dd.city_id = c.id
                left join factory f on dd.factory_id = f.id " .
            $joins .
            " where nps is not null
              AND (ds.created_at BETWEEN :d1 AND :d2)" .
            $wheres .
            "group by f1, f2, f3, question1, question2, question3, question4
        ";

        $result = Yii::$app->db->createCommand($sql);

        $result->bindValue(':d1', strtotime($date0));
        $result->bindValue(':d2', strtotime('tomorrow', strtotime($date1)) - 1);
        if ($request->isPost) {
            if ($form['transportCompanyId']) {
                $result->bindValue(':transport_company_id', $form['transportCompanyId']);
            }
            if ($form['statusDeliveryId']) {
                $result->bindValue(':status_delivery_id', $form['statusDeliveryId']);
            }
            if ($form['timeRangeName']) {
                $result->bindValue(':time_range', $form['timeRangeName']);
            }
            if ($form['factoryId']) {
                $result->bindValue(':factory_id', $form['factoryId']);
            }
            if ($form['areaId']) {
                $result->bindValue(':area_id', $form['areaId']);
            }
        }

        $result = $result->queryAll();

        foreach ($result as $item) {
            if (!key_exists($item['f1'], $data['items']))
                $data['items'][$item['f1']] = ['data' => $startTempl, 'items' => []];
            if (!key_exists($item['f2'], $data['items'][$item['f1']]['items']))
                $data['items'][$item['f1']]['items'][$item['f2']] = ['data' => $startTempl, 'items' => []];
            if (!key_exists($item['f3'], $data['items'][$item['f1']]['items'][$item['f2']]['items']))
                $data['items'][$item['f1']]['items'][$item['f2']]['items'][$item['f3']] = ['data' => $startTempl];

            foreach ($startTempl as $key => $val) {
                $data['items'][$item['f1']]['data'][$key] += $item[$key];
                $data['items'][$item['f1']]['items'][$item['f2']]['data'][$key] += $item[$key];
                $data['items'][$item['f1']]['items'][$item['f2']]['items'][$item['f3']]['data'][$key] += $item[$key];
            }
        }

        return $this->render('index', [
            'id' => Yii::$app->controller->id,
            'filters' => [
                'transportCompanyId' => $form ? $form['transportCompanyId'] : '',
                'deliveryDate' => $date0 . ' - ' . $date1,
                'statusDeliveryId' => $form ? $form['statusDeliveryId'] : '',
                'timeRangeName' => $form ? $form['timeRangeName'] : '',
                'factoryId' => $form ? $form['factoryId'] : '',
                'areaId' => $form ? $form['areaId'] : '',
            ],
            'data' => $data,
        ]);
    }

}
