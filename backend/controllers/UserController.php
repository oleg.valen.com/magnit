<?php

namespace backend\controllers;

use backend\models\SurveyUploadForm;
use common\models\Area;
use common\models\Client;
use common\models\DeliveryDoc;
use common\models\Store;
use common\models\Survey;
use common\models\DeliverySurveySearch;
use common\models\ClientToken;
use common\models\Factory;
use common\models\TransportCompany;
use common\models\User;
use common\services\SmsService;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Yii;
use yii\web\UploadedFile;

class UserController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create'],
                'rules' => [
//                    [
//                        'actions' => ['create'],
//                        'allow' => true,
//                    ],
                    [
                        'actions' => ['index', 'create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {

        $users = User::find()->where(['!=', 'status', User::STATUS_DELETED])->all();

        return $this->render('index', [
            'id' => Yii::$app->controller->id,
            'users' => $users,
            'areas' => Area::find()->indexBy(['id'])->all(),
            'stores' => Store::find()->indexBy(['id'])->all(),
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate()
    {
        $request = Yii::$app->request;

        $model = new User();
        $model->status = User::STATUS_ACTIVE;
        if ($request->isPost) {
            if ($model->load($request->post())) {
                $model->status = array_key_exists('status', $this->request->post()['User']) ? User::STATUS_ACTIVE : User::STATUS_INACTIVE;
                if ($model->validate()) {
                    $model->setPassword($this->request->post()['User']['password']);
                    $model->save(false);
                    $this->redirect(['/users']);
                }
            }
        }

        return $this->render('create', [
            'id' => Yii::$app->controller->id,
            'model' => $model,
            'areas' => Area::find()->indexBy(['id'])->all(),
            'stores' => Store::find()->indexBy(['id'])->all(),
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost) {
            if ($model->load($this->request->post())) {
                $model->status = array_key_exists('status', $this->request->post()['User']) ? User::STATUS_ACTIVE : User::STATUS_INACTIVE;
//                $model->setPassword($this->request->post()['User']['password']);

                if ($model->access == User::ROLE_GM) {
                    $model->area_id = null;
                    $model->store_id = $model->store_id != 0 ? $model->store_id : null;
                } elseif (in_array($model->access, [User::ROLE_BF, User::ROLE_RMKS, User::ROLE_OPER_BF])) {
                    $model->area_id = $model->area_id != 0 ? $model->area_id : null;
                    $model->store_id = null;
                } else {
                    $model->area_id = null;
                    $model->store_id = null;
                }

                $model->save();
                return $this->redirect(['/users']);
            }
        }

        return $this->render('update', [
            'id' => Yii::$app->controller->id,
            'model' => $model,
            'areas' => Area::find()->indexBy(['id'])->all(),
            'stores' => Store::find()->indexBy(['id'])->all(),
        ]);
    }

    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->status = User::STATUS_DELETED;
        $model->save();

        return $this->redirect(['/users']);
    }

    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist . ');
    }

}
