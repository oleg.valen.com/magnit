<?php

namespace backend\controllers;

use backend\models\DeliverySurveyUploadForm;
use common\models\AnketaDelivery;
use common\models\AnketaProduct;
use common\models\DeliverySurvey;
use common\models\Survey;
use common\models\LoginForm;
use common\models\SignupForm;
use common\models\SurveyType;
use common\models\User;
use Yii;
use yii\data\Pagination;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'signup', 'login', 'logout'],
                'rules' => [
                    [
                        'actions' => ['login', 'signup'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index', 'logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'logout' => ['post'],
//                ],
//            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
////        if (in_array(Yii::$app->user->id, Yii::$app->authManager->getUserIdsByRole('logist'))) {
////            $this->redirect(['delivery-survey/index']);
////        }
//        if (Yii::$app->user->can('viewRetail') == false) {
//            $this->redirect(['delivery-survey/index']);
//        }
//
//        $deliverySurveys = Survey::find()->limit(20)->all();
//
//        return $this->render('index', [
//            'id' => Yii::$app->controller->id,
//            'deliverySurveys' => $deliverySurveys,
//        ]);

        ///////////////////////////////

        $request = Yii::$app->request;
        $post = $request->post();
        $form = null;
        $surveys = [];
        $session = Yii::$app->session;

//        $uploadModel = new DeliverySurveyUploadForm();
        $questionsBase = ['question1' => array_fill(1, 5, 0), 'question2' => array_fill(1, 5, 0)];
        $othersBase = ['percentComment' => 0, 'totalComment' => 0, 'percent' => 0, 'total' => 0];
        $questions = [
            SurveyType::CASH => array_merge(['q' => $questionsBase], $othersBase),
            SurveyType::FRUIT => array_merge(['q' => $questionsBase], $othersBase),
            SurveyType::PROD => array_merge(['q' => array_merge($questionsBase, ['question3' => array_fill(1, 5, 0)])], $othersBase),
            SurveyType::SERVICE => array_merge(['q' => $questionsBase], $othersBase),
            SurveyType::SHOPPING => array_merge(['q' => array_merge($questionsBase, ['question3' => array_fill(1, 5, 0)])], $othersBase),


//            'question1' => array_fill(1, 5, 0),
//            'question2' => array_fill(1, 3, 0),
//            'question3' => array_fill(1, 4, 0),
//            'question4' => array_fill(1, 3, 0),
        ];
//        $nps = [
//            'grades' => array_fill(0, 11, 0),
//            'detractors' => [0, 0],
//            'passives' => [0, 0],
//            'promoters' => [0, 0],
//            'total' => 0,
//        ];

        $query = Survey::find()
            ->where(['not', ['nps' => null]]);

        if ($request->isPost) {
            $form = $post['Form'];
            $dates = explode(' - ', $form['createdAt']);
            $date0 = $dates[0];
            $date1 = $dates[1];
            $query = $query->andWhere([
                'between',
                'created_at',
                strtotime($date0), strtotime('tomorrow', strtotime($date1)) - 1
            ]);
            if ($form['storeId']) $query = $query->andWhere(['store_id' => $form['storeId']]);
            if ($form['departmentId']) $query = $query->andWhere(['dep_id' => $form['departmentId']]);
        } else {
            if ($session->has('storeId'))
                $query = $query->andWhere(['store_id' => $session->get('storeId')]);
            if ($session->has('departmentId'))
                $query = $query->andWhere(['dep_id' => $session->get('departmentId')]);

            $date0 = date('01-m-Y');
            $date1 = date('d-m-Y', time());
            $query = $query->andWhere([
                'between',
                'created_at',
                strtotime($date0), strtotime('tomorrow', strtotime($date1)) - 1
            ]);
        }

//        $countQuery = clone $query;
//        $pages = new Pagination(['totalCount' => $countQuery->count()]);
//        $deliverySurveys = $query->offset($pages->offset)
//            ->limit($pages->limit)
//            ->orderBy(['survey_id' => SORT_DESC])
//            ->all();
//
//        foreach ($queryAll = $countQuery->all() as $item) {
//            $nps['grades'][$item->nps]++;
//            if (in_array($item->nps, DeliverySurvey::BAD_NPS, true)) {
//                $nps['detractors'][0]++;
//            } elseif (in_array($item->nps, DeliverySurvey::PASSIVE_NPS, true)) {
//                $nps['passives'][0]++;
//            } elseif (in_array($item->nps, DeliverySurvey::GOOD_NPS, true)) {
//                $nps['promoters'][0]++;
//            }
//
//            foreach (range(1, 4) as $i) {
//                if ($item->{"question{$i}"} != null) {
//                    $questions["question{$i}"][$item->{"question{$i}"}]++;
//                }
//            }
//        }
//        unset($item);
//
//        $nps['detractors'][1] = count($queryAll) != 0 ? number_format($nps['detractors'][0] / count($queryAll) * 100, 2, '.', '') : 0;
//        $nps['passives'][1] = count($queryAll) != 0 ? number_format($nps['passives'][0] / count($queryAll) * 100, 2, '.', '') : 0;
//        $nps['promoters'][1] = count($queryAll) != 0 ? number_format($nps['promoters'][0] / count($queryAll) * 100, 2, '.', '') : 0;
//        $nps['total'] = count($queryAll) != 0 ? round(($nps['promoters'][0] - $nps['detractors'][0]) / count($queryAll) * 100) : 0;
//
//        foreach ($questions as &$question) {
//            $k = floor(max($question) / 100) + 1;
//            $scale = $k * 100;
//            $step = $scale / 10;
//            foreach (range(1, 9) as $i) {
//                $question['scale'][] = $scale;
//                $scale -= $step;
//            }
//            $question['k'] = $k;
//        }
//        unset($question, $item);

        foreach ($query->all() as $item) {
            foreach (range(1, 4) as $i) {
                if ($item->{"question{$i}"} != null) {
                    $questions[$item->survey_type_id]['q']["question{$i}"][$item->{"question{$i}"}]++;
                }
            }
            $questions[$item->survey_type_id]['totalComment'] += $item->comment ? 1 : 0;
            $questions[$item->survey_type_id]['total']++;
        }

        return $this->render('index', [
            'id' => Yii::$app->controller->id,
            'questions' => $questions,
//            'pages' => $pages,
            'filters' => [
                'createdAt' => $date0 . ' - ' . $date1,
                'storeId' => $form ? $form['storeId'] : ($session->has('storeId') ? $session->get('storeId') : ''),
                'departmentId' => $form ? $form['departmentId'] : ($session->has('departmentId') ? $session->get('departmentId') : ''),
            ],
            'questions' => $questions,
//            'nps' => $nps,
        ]);
    }

    public function actionSurveys()
    {
        return $this->render('surveys', [
            'id' => Yii::$app->controller->id,
        ]);
    }

    public function actionOneAnket()
    {
        return $this->render('one-anket', [
            'id' => Yii::$app->controller->id,
        ]);
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post()) && $model->signup()) {
            Yii::$app->session->setFlash('success', 'Thank you for registration. Please check your inbox for verification email.');
            return $this->goHome();
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Login action.
     *
     * @return string|Response
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $this->layout = 'blank';

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post())) {
            $model->rememberMe = $model->rememberMe === 'on' ? 1 : 0;
            if ($model->login()) {
                return $this->goBack();
            }
        }

        $model->password = '';

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

}
