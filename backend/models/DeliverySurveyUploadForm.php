<?php

namespace backend\models;

use yii\base\Model;
use yii\web\UploadedFile;
use Yii;

class DeliverySurveyUploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $xlsxFile;

    public function rules()
    {
        return [
            [['xlsxFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'xls, xlsx'],
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            $path = Yii::getAlias('@backend') . '/uploads/' . $this->xlsxFile->baseName . '.' . $this->xlsxFile->extension;
            $this->xlsxFile->saveAs($path);
//            @chmod($path, 0775);
            return $path;
        } else {
            return false;
        }
    }

    public function attributeLabels()
    {
        return [
            'xlsxFile' => 'Загрузить рассылку',
        ];
    }

}
