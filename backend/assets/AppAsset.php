<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'fonts/Geometria/stylesheet.css',
        'css/daterangepicker.css',
        'css/select2.min.css',
        'css/login.css',
        'css/delivery-survey-style.css',
        'css/menu-style.css',
        'css/style.css',
    ];
    public $js = [
        'js/moment.js',
        'js/daterangepicker.js',
        'js/main.js',
        'js/select2.min.js',
        'js/jquery.drawPieChart.js',
        'js/Chart.min.js',
        'js/Chart.roundedBarCharts.js',
        'js/charts.js',
        'js/bar-charts.js',
        'js/script.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap4\BootstrapAsset',
    ];
}
