jQuery(function ($) {
    $(document).ready(function () {
        const tabs = localStorage.getItem('tabs');
        if (!tabs) {
            localStorage.setItem('tabs', 1);
        } else {
            if ($('.tabs-link-wrap .tab-link:nth-child(' + tabs + ')').length > 0) {
                let activeTab = $('.tabs-link-wrap .tab-link:nth-child(' + tabs + ')');
                setActiveTab(activeTab);
            }
        }
        $('.tabs-link-wrap .tab-link').click(function (e) {
            localStorage.setItem('tabs', $(this).index() + 1);
            setActiveTab($(this));
        });

        function setActiveTab(activeTab) {
            activeTab.parents('.tabs-link-wrap').find('.tab-link').removeClass('active');
            activeTab.addClass('active');
            var openBlock = activeTab.attr('data-link');
            activeTab.parents('.content').find('.tab-content').fadeOut(0);
            activeTab.parents('.content').find('.tab-content').removeClass('active');
            $('.' + openBlock).fadeIn(300);
            $('.' + openBlock).addClass('active');
        }

        //progress circle for pool
        let progress = $('.progress-cirlce');
        for (let i = 0; i < progress.length; i++) {
            var percentageComplete = parseInt($(progress[i]).attr('data-percent')) / 100;
            var strokeDashOffsetValue = 100 - (percentageComplete * 100);
            var progressBar = $(progress[i]).find('.js-progress-bar');
            progressBar.css('stroke-dashoffset', strokeDashOffsetValue);
        }

        $('.toggle-sidebar').click(function () {
            $('.sidebar').toggleClass('hide');
            $('.header').toggleClass('full-width');
            $('.content').toggleClass('full-width');
        });

        $('.treeview a').click(function (e) {
            if ($(this).parents('.hidden-menu').length === 0) {
                e.preventDefault();
                $(this).parents('.treeview').toggleClass('open');
            }
        });

        $(window).on('scroll', function () {
            if ($(this).scrollTop() > 100) {
                $('.scrollup').fadeIn();
            } else {
                $('.scrollup').fadeOut();
            }
        });

        $('.scrollup').on('click', function () {
            $("html, body").animate({
                scrollTop: 0
            }, 600);
            return false;
        });

        $('.page-content').on('click', '.open-next-row', function (e) {
            let nextEl = $(this).parents('tbody').nextAll();
            let closed = $(this).hasClass('open');
            if (closed) {
                $(this).removeClass('open');
            } else {
                $(this).addClass('open');
            }
            for (let i = 0; i < nextEl.length; i++) {
                if ($(nextEl[i]).hasClass('shown-row')) {
                    return true;
                }
                if (closed) {
                    if ($(nextEl[i]).hasClass('hidden-row-1')) {
                        $(nextEl[i]).fadeOut();
                        if ($(nextEl[i]).find('.open-next-row-2.open').length > 0) {
                            console.log($(nextEl[i]).find('.open-next-row-2'));
                            $(nextEl[i]).find('.open-next-row-2.open').click();
                        }
                    }
                } else {
                    if ($(nextEl[i]).hasClass('hidden-row-1')) {
                        $(nextEl[i]).fadeIn();
                    }
                }
            }
        });

        $('.page-content').on('click', '.open-next-row-2', function (e) {
            let nextEl = $(this).parents('tbody').nextAll();
            let closed = $(this).hasClass('open');
            if (closed) {
                $(this).removeClass('open');
            } else {
                $(this).addClass('open');
            }
            for (let i = 0; i < nextEl.length; i++) {
                if ($(nextEl[i]).hasClass('shown-row') || $(nextEl[i]).hasClass('hidden-row-1')) {
                    return true;
                }
                if (closed) {
                    if ($(nextEl[i]).hasClass('hidden-row-2')) {
                        $(nextEl[i]).fadeOut();
                    }
                } else {
                    if ($(nextEl[i]).hasClass('hidden-row-2')) {
                        $(nextEl[i]).fadeIn();
                    }
                }
            }
        });

        //filter data
        if ($.fn.daterangepicker) {
            $('.box-date input[type=text]').daterangepicker({
                opens: 'rigth',
                locale: {
                    format: 'DD-MM-YYYY',
                    applyLabel: 'Принять',
                    cancelLabel: 'Отмена',
                    invalidDateLabel: 'Выберите дату',
                    daysOfWeek: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
                    monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                    firstDay: 1,
                }
            }, function (start, end, label) {
            });
        }


        //filters change
        $('.page-content').on('change', '.filter-wrapper select', function (e) {
            $(this).parents('form').submit();
        });

        //filters data change 
        $('.page-content').on('apply.daterangepicker', '.box-date input', (e, picker) => {
            $(e.target).parents('form')[0].submit();
        });
        if ($.fn.select2) {
            $('.select2-single').select2({
                minimumResultsForSearch: Infinity
            });
        }
    });

    //one survey--------------------------------------------------------------------------------
    //delivery survey
    $('.show-minute').click(function (e) {
        e.preventDefault();
        $('.first-question').fadeOut(0);
        $('.minute-cont').fadeIn(300);
    });
    $('.show-survey').click(function (e) {
        // e.preventDefault();
        $('.minute-cont').fadeOut(0);
        $('.deliver-survey').fadeIn(300);
    });

    //change for main question
    $('.first-question').on('change', 'input[type=radio]', function (e) {
        $(this).parents('.first-question').find('.hidden-btn').removeClass('hidden-btn');
        $(this).parents('.first-question').find('.comment-wrap').removeClass('hidden');
        let answer = $(this).val();
        showTextForRange(answer);
    });

    //change for main question
    $('.first-question').on('change', 'input[type=range]', function (e) {
        $(this).parents('.first-question').find('.hidden-btn').removeClass('hidden-btn');
        $(this).parents('.first-question').find('.comment-wrap').removeClass('hidden');
        let answer = $(this).val();
        showTextForRange(answer);
    });

    //show text for main question
    function showTextForRange(answer) {
        $('.text-to-range').fadeOut(0);
        if (answer > 8) {
            $('.text-9-10').fadeIn(300);
        } else if (answer < 9 && answer > 6) {
            $('.text-7-8').fadeIn(300);
        } else {
            $('.text-0-6').fadeIn(300);
        }
    }

    //move to the next question
    $('.survey-list').on('change', 'input[type=radio]', function (e) {
        let nextQuestion = $(this).parents('.question-wrap').next();
        setTimeout(() => {
            nextQuestion.get(0).scrollIntoView({
                behavior: 'smooth'
            });
        }, 500);
    });

    //move to the next question
    $('.deliver-survey').on('change', 'input[type=radio]', function (e) {
        let nextQuestion = $(this).parents('.question-wrap').next();
        if (nextQuestion.length > 0) {
            setTimeout(() => {
                nextQuestion.get(0).scrollIntoView({
                    behavior: 'smooth'
                });
            }, 500);
        }
    });

    //height of window
    var intFrameHeight = $(window).height();
    if (intFrameHeight < 601) {
        $('body').addClass('height600');
    }
    if (intFrameHeight < 651) {
        $('body').addClass('height650');

    }
    if (intFrameHeight < 701) {
        $('body').addClass('height700');

    }
    if (intFrameHeight < 751) {
        $('body').addClass('height750');

    }
    //autoheight for textarea
    $("textarea").each(function () {
        this.setAttribute("style", "height:" + (this.scrollHeight) + "px;overflow-y:hidden;");
    }).on("input", function () {
        this.style.height = "auto";
        this.style.height = (this.scrollHeight) + "px";
    });

    //validation
    var formValid = document.getElementsByClassName('form-valid')[0];
    $('.form-valid').on('click', '.btn-submit', function (e) {
        $(this).parents('form').submit(function (e) {
            e.preventDefault();
            var erroreArrayElemnts = [];
            var el = document.querySelectorAll('.form-valid input[type="radio"][data-reqired]');
            for (var i = 0; i < el.length; i++) {
                if (el[i].tagName === 'INPUT') {
                    var name = el[i].getAttribute('name');
                    if (document.querySelectorAll(`[name='${name}']:checked`).length === 0) {
                        erroreArrayElemnts.push(el[i]);
                        $(el[i]).parents('.question-wrap').addClass('has-error');
                        $(el[i]).parents('.question-wrap').find('.error-text').fadeIn(300);
                        var inputname = $(el[i]).attr('name');
                        // $('input[name='+ inputname + ']').change(function (e) {
                        $(`input[name='${inputname}']`).change(function (e) {
                            $(this).parents('.question-wrap').removeClass('has-error');
                            $(this).parents('.question-wrap').find('.error-text').fadeOut(300);
                        });
                    }
                }
            }

            if (erroreArrayElemnts.length == 0) {
                formValid.submit();
            }
            if (erroreArrayElemnts.length > 0) {
                console.log('Valid error');
                erroreArrayElemnts.sort(function (a, b) {
                    return parseInt($(a).parents('.question-wrap').offset().top) - parseInt($(b).parents('.question-wrap').offset().top)
                });
                setTimeout(() => {
                    $(erroreArrayElemnts[0]).parents('.question-wrap').get(0).scrollIntoView({
                        behavior: 'smooth'
                    });
                }, 100);
            }
        });
    });

    let timeInterval = $('.time-interval');
    setInterval(function () {
        let delta = Math.abs(Date.now() / 1000 - timeInterval.data('statusUpdatedAt'));
        let days = Math.floor(delta / 86400);
        delta -= days * 86400;
        let hours = Math.floor(delta / 3600) % 24;
        delta -= hours * 3600;
        let minutes = Math.floor(delta / 60) % 60;
        delta -= minutes * 60;
        minutes = minutes < 10 ? '0' + minutes : minutes;
        let seconds = Math.round(delta % 60);
        seconds = seconds < 10 ? '0' + seconds : seconds;
        timeInterval.text(hours + ':' + minutes + ':' + seconds + ' / ' + '2:00:00');
    }, 1000);

    $('.revision-comment textarea').on('input', function () {
        if ($(this).val().length > 6) {
            $('.confirmation-send').show();
        }
    });

});


/* one-anket-page */


// change profile image
$(".edit").click(function (e) {
    $("#imageUpload").click();
});

function photoPreview(uploader) {
    if (uploader.files && uploader.files[0]) {
        $('#profileImage').attr('src',
            window.URL.createObjectURL(uploader.files[0]));
    }
}

$("#imageUpload").change(function () {
    photoPreview(this);
});


let surveyPoint = $('.point-square .point').text() * 1;
let form = $('#anket-form');

if (surveyPoint <= 6) {
    $('.take-to-work').show();
}

$('.take-to-work').on('click', function (e) {
    e.preventDefault();

    if (!this.classList.contains('first-press')) {
        this.classList.add('first-press');
        document.querySelector('form').querySelector('input[name="mode"]').value = 'taken';
        $('.comment-container').fadeIn();
        return;
    }

    let self = $(this);
    let data = 'mode=' + encodeURIComponent($('form').find('input[name="mode"]').data('value'))
        + '&revision-comment=' + encodeURIComponent(self.closest('.comment-container').find('.comment-field').val());
    $.ajax({
        url: window.location.href,
        type: 'POST',
        data: data,
        success: function (e) {
            location.reload();
            // self.hide();
            // $('.complete-btn-wrapper').show();
        },
        error: function (e) {
            console.log(e.responseText);
        }
    });
    // return false;
});

$('.complete-btn-wrapper').on('click', function (e) {
    e.preventDefault();

    if (!this.classList.contains('first-press')) {
        this.classList.add('first-press');
        document.querySelector('form').querySelector('input[name="mode"]').value = 'closed';
        $('.comment-container').fadeIn();
        return;
    }

    let self = $(this);
    let data = 'mode=' + encodeURIComponent($('form').find('input[name="mode"]').data('value'))
        + '&revision-comment=' + encodeURIComponent(self.closest('.comment-container').find('.comment-field').val());
    $.ajax({
        url: window.location.href,
        type: 'POST',
        data: data,
        success: function (e) {
            location.reload();
            // self.hide();
            // $('.complete-btn-wrapper').show();
        },
        error: function (e) {
            console.log(e.responseText);
        }
    });
    // return false;
});

$('.accepted,.revision').on('click', function (e) {
    e.preventDefault();
    let self = $(this);
    if ($('.revision-comment').is(':hidden')) {
        $('.revision-comment').show();
        if (self.data('mode') == 'accepted') {
            $('.revision-comment p').text('Принят');
            $('.confirmation-send').data('mode', 'accepted');
        } else if (self.data('mode') == 'revision') {
            $('.revision-comment p').text('На доработку');
            $('.confirmation-send').data('mode', 'revision');
        }
    }
});

$('.confirmation-send').on('click', function (e) {
    e.preventDefault();
    let self = $(this);
    let data = 'mode=' + encodeURIComponent(self.data('mode'))
        + '&revision-comment=' + encodeURIComponent(self.closest('.comment-container').find('.comment-field').val());
    $.ajax({
        url: window.location.href,
        type: 'POST',
        data: data,
        success: function (e) {
            // self.closest('.comment-container').hide();
            location.reload();
        },
        error: function (e) {
            console.log(e.responseText);
        }
    });
    // return false;
});

// $('.complete-btn-wrapper').on('click', function () {
//     $(this).hide();
//     $('.comment-container').fadeIn();
// });

// $('.send-btn').on('click', function (evt) {
//     evt.preventDefault();
//     let comment = $('.comment-field').val();
//     if (comment == '') {
//         $('.comment-container .comment-error').fadeIn();
//     } else {
//         let data = 'comment=' + encodeURIComponent(comment)
//             + '&mode=' + encodeURIComponent('closed');
//         $.ajax({
//             url: window.location.href,
//             type: 'POST',
//             data: data,
//             success: function (e) {
//                 $('.alert-comment').text(comment)
//                 $('.comment-container').hide();
//                 $('.complete-btn-wrapper').hide();
//                 $('.alert-history-container').fadeIn();
//                 // form.submit();
//             },
//             error: function (e) {
//                 console.log(e.responseText);
//             }
//         });
//     }
// });

$('.comment-field').focus(function () {
    $('.comment-container .comment-error').fadeOut();
});
