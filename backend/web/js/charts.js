jQuery(function ($) {
    $(document).ready(function () {
        if(typeof chartData != 'undefined'){
            if(chartData.length > 0){
                DrawCharts(chartData);
            }
        }
        let windowWidth = window.innerWidth;
        $( window ).resize(function() {
            if(Math.abs(windowWidth - window.innerWidth) > 50){
                windowWidth = window.innerWidth;
                if(typeof chartData != 'undefined'){
                    if(chartData.length > 0){
                        ClearChart(chartData);
                        DrawCharts(chartData);
                    }
                }
            }
        });
        function ClearChart(chartData){
            $('.doughnutTipExpand').remove();
            $('.pyraamidTip').remove();
            $('.doughnutTip').remove();
            $('.pieTip').remove();
            for(let i = 0; i < chartData.length; i++){
                if(chartData[i].type !== 'radialbar' && chartData[i].type !== 'radialBar2'){
                    $(chartData[i].element).html(' ');
                    $(chartData[i].element).parents('.chart-content').find('.legend .legend-list').html(' ');
                    $(chartData[i].element).parents('.chart-content').find('.legend-wrap').remove();
                }
            }
        }
        function DrawCharts(chartData){
            for(let i = 0; i < chartData.length; i++){
                if(chartData[i].element && chartData[i].data){
                    if(chartData[i].type == 'lineDot'){
                        drawLineDot(chartData[i].element, chartData[i].data, chartData[i].dotColor);
                    }
                    if(chartData[i].type == 'lineChart'){
                        drawLineChart(chartData[i].element, chartData[i].data);
                    }
                    if(chartData[i].type == 'pieRound'){
                        drawPieRound(chartData[i].element, chartData[i].data, chartData[i].innerValue, chartData[i].innerText );
                    }
                    if(chartData[i].type == 'horizontalbar'){
                        drawHorizontalBar(chartData[i].element, chartData[i].data);
                    }
                    if(chartData[i].type == 'shadowLine'){
                        drawShadowLine(chartData[i].element, chartData[i].data, chartData[i].borderColor);
                    }
                }
            }
        }
        function drawLineChart(element, data) {
            if(data.length > 0 && $(element).length>0) {
                var maxValue = parseInt(data[0].progress);
                var minValue = parseInt(data[0].progress);
                var total = 0;
                for (let i = 0; i < data.length; i++) {
                    total += parseInt(data[i].progress);
                    if(maxValue<data[i].progress){
                        maxValue = parseInt(data[i].progress);
                    }
                    if(minValue>data[i].progress){
                        minValue = parseInt(data[i].progress);
                    }
                }
                var maxAxes = 1;
                var minAxes = 0;
                var axes = new Array();
                var step = 1;
                if(maxValue < 1) {
                    maxAxes = Math.round(maxValue*10)/10 + 0.1;
                    step = 0.1;
                }
                else if(maxValue < 10) {
                    maxAxes = Math.round(maxValue)/ + 1;
                    step = 1;
                }
                else {
                    step = 5;
                    while ((maxValue + step)/step > 9 || step>10000){
                        if(step >=200) {
                            step += 100;
                        }
                        else if(step >=50) {
                            step += 50;
                        }
                        else if(step >=30) {
                            step += 10;
                        }
                        else {
                            step += 5;
                        }
                    }
                    maxAxes = maxValue + step;
                    if(minValue > step && maxAxes > 100){
                        minAxes = step;
                    }
                }
                var axesValue = minAxes;
                for (let i = 0; axesValue < maxAxes + step; i++) {
                    axes.push(axesValue);
                    axesValue = axesValue + step;
                }
            
                var percentValue = new Array(data.length);
                var percentPosition = new Array(data.length);
            
                var minValuePosition = axes[0];
                var maxValuePosition = axes[axes.length-1];
                var axesRange = maxValuePosition - minValuePosition;
                var positionPercent = 100/axesRange;
                var valuePercent = 100/total;
            
                for (let i = 0; i < data.length; i++) {
                    if(parseInt(data[i].progress) > 0){
                        percentValue[i] = Math.round(valuePercent*parseInt(data[i].progress));
                        percentPosition[i] = Math.round(positionPercent*(parseInt(data[i].progress) - minValuePosition));
                    }
                    else {
                        percentValue[i] = 0;
                        percentPosition[i] = 0;
                    }
                }
        
                let tooltipValue = ' ';
                var str = '<div class="lineChartcont">';
                str += '<div class="lineChartlist">';
                for (let i = 0; i < data.length; i++) {
                    let lineStyle = 'background: linear-gradient(317.7deg, rgba(0, 0, 0, 0.4) 0%, rgba(255, 255, 255, 0.4) 105.18%), ' + data[i].background + ';';
                    lineStyle += 'box-shadow: inset -2.5px -2.5px 5px rgba(250, 251, 255, 0.1), inset 2.5px 2.5px 5px '+ data[i].background2 +';'; 
                    str += 
                    '<div class="lineChartRow">'
                    +'  <div class="line-col">'
                    +'      <div class="line">'
                    +'          <div class="active-line" style="width: '+ percentPosition[i] + '%;' + lineStyle + '"></div>'
                    +'      </div>'
                    +'  </div>'
                    +'   <div class="label-row">'
                    +'      <div class="label">' + data[i].labelText  +'</div>'
                    +'      <div class="value">'+ data[i].progress + ' шт. / ' + percentValue[i] + '%'
                    +'      </div>'
                    +'  </div>'
                    +'</div>';
                }
                str += '</div>';
                str += '<div class="y-axis">';
                for (let i = 0; i < axes.length; i++) {
                    str +=
                    '<div class="axis-item">'
                    +'    ' + axes[i] + '<br>'
                    +'  шт'
                    +'</div>';
                }
                str += '</div>';
                str += '</div>';
                $(element).append(str);
            }
        }
        function drawLineDot(element, data, dotColor) {
            if(data.length > 0 && $(element).length>0) {
                var maxValue = parseInt(data[0].progress);
                var minValue = parseInt(data[0].progress);
                var total = 0;
                for (let i = 0; i < data.length; i++) {
                    total += parseInt(data[i].progress);
                    if(maxValue<data[i].progress){
                        maxValue = parseInt(data[i].progress);
                    }
                    if(minValue>data[i].progress){
                        minValue = parseInt(data[i].progress);
                    }
                }
                var maxAxes = 1;
                var minAxes = 0;
                var axes = new Array();
                var step = 1;
                if(maxValue < 1) {
                    maxAxes = Math.round(maxValue*10)/10 + 0.1;
                    step = 0.1;
                }
                else if(maxValue < 10) {
                    maxAxes = Math.round(maxValue)/ + 1;
                    step = 1;
                }
                else {
                    step = 5;
                    while ((maxValue + step)/step > 9 || step>10000){
                        if(step >=200) {
                            step += 100;
                        }
                        else if(step >=50) {
                            step += 50;
                        }
                        else if(step >=30) {
                            step += 10;
                        }
                        else {
                            step += 5;
                        }
                    }
                    maxAxes = maxValue + step;
                    if(minValue > step && maxAxes > 100){
                        minAxes = step;
                    }
                }
                var axesValue = minAxes;
                for (let i = 0; axesValue < maxAxes + step; i++) {
                    axes.push(axesValue);
                    axesValue = axesValue + step;
                }
            
                var percentValue = new Array(data.length);
                var percentPosition = new Array(data.length);
            
                var minValuePosition = axes[0];
                var maxValuePosition = axes[axes.length-1];
                var axesRange = maxValuePosition - minValuePosition;
                var positionPercent = 100/axesRange;
                var valuePercent = 100/total;
            
                for (let i = 0; i < data.length; i++) {
                    if(parseInt(data[i].progress) > 0){
                        percentValue[i] = Math.round(valuePercent*parseInt(data[i].progress));
                        percentPosition[i] = Math.round(positionPercent*(parseInt(data[i].progress) - minValuePosition));
                    }
                    else {
                        percentValue[i] = 0;
                        percentPosition[i] = 0;
                    }
                }
        
                let tooltipValue = ' ';
                var str = '<div class="lineDotcont">';
                str += '<div class="lineDotlist">';
                for (let i = 0; i < data.length; i++) {
                    var label = 'баллов';
                    if($.isNumeric(data[i].labelText)) {
                        if(data[i].labelText == '1'){
                            label = 'балл';
                        }
                        else if(data[i].labelText == '2' || data[i].labelText == '3' || data[i].labelText == '4'){
                            label = 'балла';
                        }
                    }
                    else {
                        label = ' ';
                    }
                    str += 
                    '<div class="lineDotRow">'
                    +'    <div class="label">' + data[i].labelText + ' ' + label
                    + tooltipValue
                    +'</div>'
                    +'  <div class="line-col">'
                    +'      <div class="line" style="background: linear-gradient(90deg, '+ data[i].backgroundStart + ' 0%, '+ data[i].backgroundEnd + ' 100%);">'
                    +'          <div class="dot" style="left: calc('+ percentPosition[i] + '% - 7px);border-color: '+ dotColor +'"></div>'
                    +'          <div class="tooltip" style="left: calc('+ percentPosition[i] + '% - 50px);">' + percentValue[i] + '% / ' + data[i].progress + ' шт</div>'
                    +'      </div>'
                    +'  </div>'
                    +'</div>';
                }
                str += '</div>';
                str += '<div class="y-axis">';
                for (let i = 0; i < axes.length; i++) {
                    str +=
                    '<div class="axis-item">'
                    +'    ' + axes[i] + '<br>'
                    +'  шт'
                    +'</div>';
                }
                str += '</div>';
                str += '</div>';
                $(element).append(str);
            }
        }
        function drawPieRound(element, data, innerValue, innerText){
            if($(element).length==1 && data.length > 0){
                var newData = data;
                var total = 0;
                $(element).addClass('rounded');
                for (let i = 0; i < data.length; i++) {
                    newData[i].title = data[i].labelText;
                    newData[i].color = data[i].background;
                    newData[i].value = parseInt(data[i].progress);
                    total = total + parseInt(data[i].progress);
                }
                if(newData) {
                    if(total > 0) {
                        $(element).drawPieChart(newData);
                    }
                    drawLegend(element, data);
                    if(innerText && innerValue){
                        addCenterText(element, innerValue, innerText);
                    }
                }
            }
        }
        function drawLegend(element, data){
            if($(element).parents('.pieRound').find('.legend-wrap').length === 0){
                let legendHtml = '<div class="legend-wrap"><div class="legend-list"></div></div>';
                $(legendHtml).appendTo($(element).parents('.pieRound'));
            }
            let legend = $(element).parents('.pieRound').find('.legend-wrap .legend-list');
            let segmentTotal = 0;
            for (let i = 0, len = data.length; i < len; i++){
                if(data[i].value) {
                    segmentTotal += data[i].value;
                }
            }
            //percent for each value
            for (let i = 0, len = data.length; i < len; i++){
                if(data[i].value) {
                    data[i].percent = Math.round((100/segmentTotal)*data[i].value);
                }
                else {
                    data[i].percent = 0;
                }
            }
            for (let i = 0, len = data.length; i < len; i++){
                let legendRow = 
                '<div class="legend-item">'
                +'    <div class="circle" style="background: '+ data[i].color +'; box-shadow: 0px 7px 8px -1px '+ data[i].color +'50;"></div>'
                +'  <div class="label-col">'
                +'      <div class="value">'
                + data[i].value +' шт. / ' + data[i].percent + '%'
                +'      </div>'
                +'      <div class="label">'
                + data[i].title
                +'      </div>'
                +'  </div>'
                +'</div>';
                $(legendRow).appendTo(legend);
            }
        }
        function addCenterText(element, value, text){
            if($(element).length > 0 && value && text){
                let innerText = 
                '<div class="inner-circle">'
                +'<div class="inner-text">'
                + '<div class="value">'
                + value
                + '</div>'
                + '<div class="text">'
                + text
                + '</div>'
                + '</div>'
                + '</div>';
                $(innerText).appendTo(element);
            }
        }
        function drawHorizontalBar(element, data){
            if($(element).length==1 && data.length > 0){
                if(data[0].labelText !== '5') {
                    data.reverse();
                }
                var id = element.split('.')[1];
                var width = 600;
                var height = 230;
                if(window.screen.width > 992 && window.screen.width <= 1200){
                    width = 400;
                    height = 240;
                }
                else if(window.screen.width > 768 && window.screen.width <= 992){
                    width = 600;
                    height = 230;
                }
                else if(window.screen.width > 500 && window.screen.width <= 768) {
                    width = 750;
                    height = 250;
                }
                else if(window.screen.width <= 500) {
                    width = 350;
                    height = 350;
                }
                var canvas = '<canvas  id="' + id + '" style="height: '+ height + 'px; width: '+ width +'px;"></canvas>';
                $(canvas).appendTo($(element));        
                var newData = new Array(data.length);
                var backgroundColor = new Array(data.length);
                var labels = new Array(data.length);
                var maxValue = parseInt(data[0].progress);
                for (let i = 0; i < data.length; i++) {
                    var label = 'баллов';
                    if($.isNumeric(data[i].labelText)) {
                        if(data[i].labelText == '1'){
                            label = 'балл';
                        }
                        else if(data[i].labelText == '2' || data[i].labelText == '3' || data[i].labelText == '4'){
                            label = 'балла';
                        }
                    }
                    else {
                        label = ' ';
                    }
        
                    labels[i] = data[i].labelText + ' ' + label;
                    backgroundColor[i] = data[i].background;
                    newData[i] = data[i].progress;
                    if(maxValue<parseInt(data[i].progress)) {
                        maxValue = parseInt(data[i].progress);
                    }
                }
                maxValue = Math.round(maxValue + maxValue/5);
                if(maxValue>500){
                    maxValue = Math.round(maxValue*100)/100;
                }
                else if(maxValue>100){
                    maxValue = Math.round(maxValue*50)/50;
                }
                var ChartData = {
                    labels: labels,
                    datasets: [{
                        data: newData,
                        backgroundColor: backgroundColor,
                        hoverBackgroundColor: backgroundColor,
                        borderWidth: 0
                    }]
                };
        
                var horizontal = document.getElementById(id).getContext('2d');
                var fontSize = 10;
                var myBarChart = new Chart(horizontal, {
                    type: 'horizontalBar',
                    data: ChartData,
                    options: {
                        cornerRadius: 100,
                        cornerposition: 'right', //right, left, bottom, top, all
                        tooltips: {
                            mode: 'nearest',
                            backgroundColor: '#C9C9C9',
                            titleFontSize: 8,
                            titleAlign: 'center',
                            position: 'average',
                            xPadding: 10,
                            yPadding: 5,
                            cornerRadius: 10,
                            displayColors: false,
                            fontSize: fontSize,
                            // yAlign: 'bottom',
                            // xAlign: 'center',
                            callbacks: {
                                title: function() {},
                                label: function(tooltipItem, data) {
                                    var values = data.datasets[tooltipItem.datasetIndex].data;
                                    var total = 0;
                                    for(let i = 0; i < values.length; i++){
                                        total += parseInt(values[i]);
                                    }
                                    var percent;
                                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index])>0){
                                        percent = Math.round((100/total)*data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]);
                                    }
                                    else {
                                        percent = 0;
                                    }
                                    var label = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index] || '';
                                    label =  percent + '% / '+ label + ' шт';
                                    return label;
                                }
                            }
                        },
                        hover: {mode: null},
                        legend: {
                            display: false,
                        },
                        layout: {
                            padding: {
                                left: 50,//for text-align
                            }
                        }, 
                        scales: {
                            xAxes: [{
                                gridLines: {
                                    color: '#D5D3D3',
                                    lineWidth: 1
                                },
                                ticks: {
                                    fontSize: fontSize,
                                    min: 0,
                                    max: maxValue,
                                    beginAtZero: true,
                                    callback: function(value, index, values) {
                                        var axesValue = Math.round(value*10)/10;
                                        if(index==values.length-1){
                                            return ' ';
                                        }
                                        else {
                                            return [' ' , axesValue,  'шт'];
                                        }
                                    },
                                }
                            }],
                            yAxes: [{
                                gridLines: {
                                    color: '#D5D3D3',
                                    lineWidth: 1
                                },
                                ticks: {
                                    fontSize: fontSize,
                                    mirror: true,//for text-align
                                    padding: 50//for text-align
                                }
                            }],
                        },
                    },
                    plugins: [{
                        afterDraw: function(horizontal){    
                            var ctx = horizontal.chart.ctx; 
                            var yAxis = horizontal.scales['y-axis-0'];
                            var xAxis = horizontal.scales['x-axis-0'];
                            var bottom = yAxis.bottom;
                            //y-axis line
                            for (var index = 0; index < yAxis.ticks.length; index++) {
                                var y = yAxis.getPixelForTick(index);  
                                if (!window.document.documentMode) {
                                    if(index != 0){
                                        ctx.beginPath();
                                        ctx.moveTo(15,  y + 0.5);
                                        ctx.lineTo(yAxis.right,  y + 0.5);
                                        ctx.lineWidth = 1;
                                        ctx.strokeStyle = "#D5D3D3";
                                        ctx.stroke();
                                    }
                                }
                            }
                            ctx.beginPath();
                            ctx.moveTo(15,  bottom + 0.5);
                            ctx.lineTo(yAxis.right,  bottom + 0.5);
                            ctx.lineWidth = 1;
                            ctx.strokeStyle = "#D5D3D3";
                            ctx.stroke();
                        }
                    }],
                });
            }
        }
        function drawShadowLine(element, data, borderColor) {
            //type shadowLine
            (function()
            {
                var ShadowLineElement = Chart.elements.Line.extend({
                    draw: function()
                    {
                        var ctx = this._chart.ctx;
                        var vm = this._view;
                        var borderColor = vm.borderColor;
                        var originalStroke = ctx.stroke;
                        ctx.stroke = function()
                        {
                            ctx.save();
                            ctx.shadowColor = borderColor;
                            ctx.shadowBlur = 4;
                            ctx.shadowOffsetX = 0;
                            ctx.shadowOffsetY = 0;
                            originalStroke.apply(this, arguments);
                            ctx.restore();
                        };
                        Chart.elements.Line.prototype.draw.apply(this, arguments);
                        ctx.stroke = originalStroke;
                    }
                });
                Chart.defaults.ShadowLine = Chart.defaults.line;
                Chart.controllers.ShadowLine = Chart.controllers.line.extend({
                    datasetElementType: ShadowLineElement
                });
            })();
            if($(element).length==1 && data.length > 0){
                if(data[0].labelText !== '5') {
                    data.reverse();
                }
                var id = element.split('.')[1];
                var width = 500;
                var height = 200;
                if(window.screen.width > 992 && window.screen.width <= 1200) {
                    width = 600;
                    height = 350;
                }
                else if(window.screen.width > 768 && window.screen.width <= 992){
                    width = 600;
                    height = 200;
                }
                else if(window.screen.width > 500 && window.screen.width <= 768) {
                    width = 750;
                    height = 250;
                }
                else if(window.screen.width <= 500) {
                    width = 350;
                    height = 200;
                }
                var canvas = '<canvas  id="' + id + '" style="height: '+ height + 'px; width: '+ width +'px;"></canvas>';
                $(canvas).appendTo($(element));
                data.reverse();   
                var newData = new Array(data.length);
                var backgroundColor = new Array(data.length);
                var labels = new Array(data.length);
                var maxValue = data[0].progress;
                var longLabels;
                for (let i = 0; i < data.length; i++) {
                    if($.isNumeric(data[i].labelText)) {
                        if(data[i].labelText == '1'){
                            longLabels = data[i].labelText + ' балл';
                        }
                        else if(data[i].labelText == '2' || data[i].labelText == '3' || data[i].labelText == '4'){
                            longLabels = data[i].labelText + ' балла';
                        } else if(parseInt(data[i].labelText) > 4) {
                            longLabels = data[i].labelText + ' баллов';
                        }
                    }
                    else {
                        let labels = data[i].labelText.split(' ');
                        let labelsLong = new Array();
                        if (labels.length > 3){
                            for (let index = 0; index < labels.length;) {
                                let labelItem = labels[index];
                                let index2 = index + 1;
                                if(labels.length > index2){
                                    labelItem += ' ' + labels[index2];
                                }
                                labelsLong.push(labelItem);
                                index = index + 2;
                            }
                            longLabels = labelsLong;
                        }
                        else {
                            longLabels =  data[i].labelText;
                        }
                    }
                    labels[i] = longLabels;
                    backgroundColor[i] = data[i].background;
                    newData[i] = data[i].progress;
                    if(maxValue<data[i].progress){
                        maxValue = data[i].progress;
                    }
                }
                maxValue = parseInt(maxValue) + 10;
                var shadowLineEl = document.getElementById(id).getContext('2d');
                gradient = shadowLineEl.createLinearGradient(0, 0, 0, height);
                gradient.addColorStop(0, borderColor);
                gradient.addColorStop(0.5, borderColor + '55');
                gradient.addColorStop(0.9, borderColor + '00');
                Chart.plugins.register({
                    beforeRender: function(chart) {
                        if (chart.config.options.showAllTooltips) {
                        // create an array of tooltips, 
                        // we can't use the chart tooltip because there is only one tooltip per chart
                        chart.pluginTooltips = [];
                        chart.config.data.datasets.forEach(function(dataset, i) {
                            chart.getDatasetMeta(i).data.forEach(function(sector, j) {
                            chart.pluginTooltips.push(new Chart.Tooltip({
                                _chart: chart.chart,
                                _chartInstance: chart,
                                _data: chart.data,
                                _options: chart.options.tooltips,
                                _active: [sector]
                            }, chart));
                            });
                        });      
                        chart.options.tooltips.enabled = false; // turn off normal tooltips
                        }
                    },
                    afterDraw: function(chart, easing) {
                        if (chart.config.options.showAllTooltips) {
                        if (!chart.allTooltipsOnce) {
                            if (easing !== 1) {
                            return;
                            }
                            chart.allTooltipsOnce = true;
                        }
                        chart.options.tooltips.enabled = true;
                        Chart.helpers.each(chart.pluginTooltips, function(tooltip) {
                            tooltip.initialize();
                            tooltip.update();
                            tooltip.pivot();
                            tooltip.transition(easing).draw();
                        });
                        chart.options.tooltips.enabled = false;
                        }
                    }
                });
                var ChartData = {
                    labels: labels,
                    datasets: [{
                        data: newData,
                        backgroundColor: gradient,
                        pointBackgroundColor: 'white',
                        borderWidth: 2,
                        borderColor: borderColor,
                    }]
                };
                var fontSize = 10;
                let showAllTooltips = false;
                var optionsLine = {
                    responsive: true,
                    maintainAspectRatio: true,
                    cutoutPercentage: 70,
                    animation: {
                        easing: 'easeInOutQuad',
                        duration: 520
                    },
                    hover: {mode: null},
                    scales: {
                        xAxes: [{
                            gridLines: {
                                color: '#D5D3D3',
                                lineWidth: 1
                            },
                            ticks: {
                                fontSize: fontSize,
                                min: 0
                            }
                        }],
                        yAxes: [{
                            barPercentage: 1.0,
                            weight: 100,
                            gridLines: {
                                color: '#D5D3D3',
                                lineWidth: 1
                            },
                            ticks: {
                                min: 0,
                                padding: 10,
                                beginAtZero: true,
                                fontSize: fontSize,
                            }
                        }],
                    },
                    elements: {
                        line: {
                            tension: 0.4,
                        }
                    },
                    legend: {
                        display: false
                    },
                    point: {
                        backgroundColor: 'white'
                    },
                    tooltips: {
                        mode: 'nearest',
                        backgroundColor: borderColor,
                        titleFontSize: 8,
                        titleAlign: 'center',
                        position: 'average',
                        xPadding: 10,
                        yPadding: 5,
                        cornerRadius: 10,
                        displayColors: false,
                        fontSize: fontSize,
                        // yAlign: 'bottom',
                        // xAlign: 'center',
                        callbacks: {
                            title: function() {},
                            label: function(tooltipItem, data) {
                                var values = data.datasets[tooltipItem.datasetIndex].data;
                                var total = 0;
                                for(let i = 0; i < values.length; i++){
                                    total += parseInt(values[i]);
                                }
                                var percent;
                                if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index])>0){
                                    percent = Math.round((100/total)*data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]);
                                }
                                else {
                                    percent = 0;
                                }
                                var label = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index] || '';
                                label =  percent + '% / '+ label + ' шт';
                                return label;
                            }
                        }
                    },
                    showAllTooltips: showAllTooltips
                };
                chartInstanceTeam = new Chart(shadowLineEl, {
                    type: 'ShadowLine',
                    data: ChartData,
                    responsive: true,
                    options: optionsLine
                });
        
            }
        }
    });
});