<?php

namespace backend\views\widgets;

use common\models\Area;
use common\models\DeliveryDoc;
use common\models\Department;
use common\models\Factory;
use common\models\Store;
use common\models\TransportCompany;
use yii\base\Widget;
use Yii;

class SiteFilterWidget extends Widget
{
    public $title;
    public $showAlerts = false;
    public $filters;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $user = Yii::$app->user->getIdentity();

        $stores = Store::find()->orderBy('name');
        if ($user->store_id)
            $stores = $stores->andWhere(['id' => $user->store_id]);
        if ($user->area_id)
            $stores = $stores->andWhere(['area_id' => $user->area_id]);

        return $this->render('_site-filter', [
            'title' => $this->title,
            'stores' => $stores->all(),
            'departments' => Department::find()->orderBy('name')->all(),
            'filters' => $this->filters,
        ]);

    }
}