<?php

namespace backend\views\widgets;

use common\models\Area;
use common\models\DeliveryDoc;
use common\models\Department;
use common\models\Factory;
use common\models\StatusDelivery;
use common\models\Store;
use common\models\TransportCompany;
use yii\base\Widget;
use Yii;

class SurveyFilterWidget extends Widget
{
    public $title;
    public $showAlerts = false;
    public $filters;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $user = Yii::$app->user->getIdentity();

        $stores = Store::find()->orderBy('name');
        if ($user->store_id)
            $stores = $stores->andWhere(['id' => $user->store_id]);
        if ($user->area_id)
            $stores = $stores->andWhere(['area_id' => $user->area_id]);

        return $this->render('_survey-filter', [
            'title' => $this->title,
            'showAlerts' => $this->showAlerts,
            'stores' => $stores->all(),
            'departments' => Department::find()->orderBy('name')->all(),
//            'transportCompanies' => TransportCompany::find()->orderBy('name')->all(),
//            'timeRange' => DeliveryDoc::find()->select('time_range')->distinct()->orderBy('time_range')->all(),
//            'statusDeliveries' => StatusDelivery::find()->orderBy('name')->all(),
            'filters' => $this->filters,
        ]);

    }
}