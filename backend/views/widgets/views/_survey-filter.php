<?php

use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Json;
use common\models\SurveyType;
?>

<div class="pink-block">
    <div class="top-nav">
        <div class="navbar-aside">
            <div class="mobile-menu btn-aside-menu toggle-sidebar">
                <div class="b-menu">
                    <div class="b-bun b-bun--top"></div>
                    <div class="b-bun b-bun--mid"></div>
                    <div class="b-bun b-bun--bottom"></div>
                </div>
            </div>
        </div>
        <div class="navbar-user">
            <div class="user">
                <div class="user-wrapper show-user-dropdown">
                    <div class="user-name"><?= Yii::$app->user->getIdentity()->surname_name ?></div>
                    <div class="icon-user"></div>
                </div>
                <div class="dropdown-user">
                    <ul>
                        <li>
                            <?= Html::a('Выход', ['site/logout'], [
                                'class' => 'logout',
                                'title' => 'Выход',
                                'data' => ['method' => 'post']]) ?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="page-header"><?= $title ?></div>
    <?php $form = ActiveForm::begin([
        'id' => 'delivery-filter-form',
        'method' => 'post',
    ]); ?>
    <div class="filter-wrapper">
        <!-- блок выбора даты -->
        <div class="box box-date">
            <h6 class="f-name-height">Дата опроса</h6>
            <div class="form-group">
                <input type="text" class="form-control" name="Form[createdAt]"
                       value="<?= $filters['createdAt'] ?>">
                <div class="help-block"></div>
            </div>
        </div>

        <!-- блок выбора расположения -->
        <div class="box">
            <h6>Магазин</h6>
            <div class="form-group">
                <select class="form-control" name="Form[storeId]">
                    <option value="">Все</option>
                    <?php foreach ($stores as $item): ?>
                        <option value="<?= $item->id ?>"<?= $filters['storeId'] == $item->id ? ' selected' : '' ?>><?= $item->name ?></option>
                    <?php endforeach; ?>
                </select>
                <div class="help-block"></div>
            </div>
        </div>

        <!-- блок выбора расположения -->
        <div class="box">
            <h6>Отдел </h6>
            <div class="form-group">
                <select class="form-control" name="Form[departmentId]">
                    <option value="">Все</option>
                    <?php foreach ($departments as $item): ?>
                        <option value="<?= $item->id ?>"<?= $filters['departmentId'] == $item->id ? ' selected' : '' ?>><?= $item->name ?></option>
                    <?php endforeach; ?>
                </select>
                <div class="help-block"></div>
            </div>
        </div>

        <!-- блок выбора расположения -->
        <div class="box">
            <h6>Опросы</h6>
            <div class="form-group">
                <select class="form-control" name="Form[surveyTypeId]">
                    <option value="">Все</option>
                    <?php foreach (SurveyType::$valueNames as $key => $name): ?>
                        <option value="<?= $key ?>"<?= $filters['surveyTypeId'] == $key ? ' selected' : '' ?>><?= $name ?></option>
                    <?php endforeach; ?>
                </select>

                <div class="help-block"></div>
            </div>
        </div>
        <input type="hidden" value=""/>
    </div>

    <?php if ($showAlerts): ?>
        <div class="alerts-wrap">
            <div class="alert-item">
                <a href="<?= Url::current() ?>" class="alert-row" data-method="post"
                   data-params='<?= Json::encode(['mode' => 'all']) ?>'>
                    <div class="info-col">
                        <div class="name">
                            Всего анкет
                        </div>
                        <div class="sum">
                            <?= $filters['die']['total'] ?> из <?= $filters['die']['totalAll'] ?>
                        </div>
                    </div>
                    <div class="icon-col">
                        <div class="icon icon-all-anket"></div>
                    </div>
                </a>
            </div>
            <div class="alert-item">
                <a href="<?= Url::current() ?>" class="alert-row" data-method="post"
                   data-params='<?= Json::encode(['mode' => 'bad-nps']) ?>'>
                    <div class="info-col">
                        <div class="name">
                            Доля Алертов
                        </div>
                        <div class="sum">
                            <?= $filters['die']['badNps'] ?> %
                        </div>
                    </div>
                    <div class="icon-col">
                        <div class="icon icon-share-alert"></div>
                    </div>
                </a>
            </div>
            <div class="alert-item">
                <a href="<?= Url::current() ?>" class="alert-row" data-method="post"
                   data-params='<?= Json::encode(['mode' => 'expired']) ?>'>
                    <div class="info-col">
                        <div class="name">
                            Просроч. Алерты
                        </div>
                        <div class="sum">
                            <?= $filters['die']['expired'] ?>
                        </div>
                    </div>
                    <div class="icon-col">
                        <div class="icon icon-expired-alert"></div>
                    </div>
                </a>
            </div>
            <div class="alert-item">
                <a href="<?= Url::current() ?>" class="alert-row" data-method="post"
                   data-params='<?= Json::encode(['mode' => 'new']) ?>'>
                    <div class="info-col">
                        <div class="name">
                            Новые Алерты
                        </div>
                        <div class="sum">
                            <?= $filters['die']['new'] ?>
                        </div>
                    </div>
                    <div class="icon-col">
                        <div class="icon icon-new-alert"></div>
                    </div>
                </a>
            </div>
            <div class="alert-item">
                <a href="<?= Url::current() ?>" class="alert-row" data-method="post"
                   data-params='<?= Json::encode(['mode' => 'taken']) ?>'>
                    <div class="info-col">
                        <div class="name">
                            Алерты в работе
                        </div>
                        <div class="sum">
                            <?= $filters['die']['taken'] ?>
                        </div>
                    </div>
                    <div class="icon-col">
                        <div class="icon icon-alert-in-work"></div>
                    </div>
                </a>
            </div>
        </div>
    <?php endif; ?>
    <?php ActiveForm::end() ?>
</div>