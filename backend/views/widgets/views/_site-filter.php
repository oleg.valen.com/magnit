<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

?>

<div class="pink-block">
    <div class="top-nav">
        <div class="navbar-aside">
            <div class="mobile-menu btn-aside-menu toggle-sidebar">
                <div class="b-menu">
                    <div class="b-bun b-bun--top"></div>
                    <div class="b-bun b-bun--mid"></div>
                    <div class="b-bun b-bun--bottom"></div>
                </div>
            </div>
        </div>
        <div class="navbar-user">
            <div class="user">
                <div class="user-wrapper show-user-dropdown">
                    <div class="user-name"><?= Yii::$app->user->getIdentity()->surname_name ?></div>
                    <div class="icon-user"></div>
                </div>
                <div class="dropdown-user">
                    <ul>
                        <li>
                            <?= Html::a('Выход', ['site/logout'], [
                                'class' => 'logout',
                                'title' => 'Выход',
                                'data' => ['method' => 'post']]) ?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="page-header"><?= $title ?></div>
    <?php $form = ActiveForm::begin([
        'id' => 'delivery-filter-form',
        'method' => 'post',
    ]); ?>
    <div class="filter-wrapper">
        <!-- блок выбора даты -->
        <div class="box box-date">
            <h6 class="f-name-height">Дата опроса</h6>
            <div class="form-group">
                <input type="text" class="form-control" name="Form[createdAt]"
                       value="<?= $filters['createdAt'] ?>">
                <div class="help-block"></div>
            </div>
        </div>

        <!-- блок выбора расположения -->
        <div class="box">
            <h6>Магазин</h6>
            <div class="form-group">
                <select class="form-control" name="Form[storeId]">
                    <option value="">Все</option>
                    <?php foreach ($stores as $item): ?>
                        <option value="<?= $item->id ?>"<?= $filters['storeId'] == $item->id ? ' selected' : '' ?>><?= $item->name ?></option>
                    <?php endforeach; ?>
                </select>
                <div class="help-block"></div>
            </div>
        </div>

        <!-- блок выбора расположения -->
        <div class="box">
            <h6>Отдел </h6>
            <div class="form-group">
                <select class="form-control" name="Form[departmentId]">
                    <option value="">Все</option>
                    <?php foreach ($departments as $item): ?>
                        <option value="<?= $item->id ?>"<?= $filters['departmentId'] == $item->id ? ' selected' : '' ?>><?= $item->name ?></option>
                    <?php endforeach; ?>
                </select>
                <div class="help-block"></div>
            </div>
        </div>
        <input type="hidden" value=""/>
    </div>
    <?php ActiveForm::end() ?>
</div>