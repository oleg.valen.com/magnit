<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use backend\views\widgets\SurveyFilterWidget;
use backend\assets\DeliveryIndicatorAsset;
use yii\helpers\Json;
use yii\web\View;

DeliveryIndicatorAsset::register($this);

$this->registerJs(
    "let barChartData = new Array();
    barChartData.push(
        {
            element: '.bar-chart-1', // class for element
            color: '#E8B5CA', //color for bar
            data: [
                {
                    progress: ".Json::htmlEncode([$questions['question1'][5]]).",
                    labelText: 'Отлично'
                },
                {
                    progress: ".Json::htmlEncode([$questions['question1'][4]]).",
                    labelText: 'Хорошо'
                },
                {
                    progress: ".Json::htmlEncode([$questions['question1'][3]]).",
                    labelText: 'Удовлетвор.'
                },
                {
                    progress: ".Json::htmlEncode([$questions['question1'][2]]).",
                    labelText: 'Плохо'
                },
                {
                    progress: ".Json::htmlEncode([$questions['question1'][1]]).",
                    labelText: 'Очень плохо'
                },
            ]
        }
    );
    barChartData.push(
        {
            element: '.bar-chart-2', // class for element
            color: '#ECCCFF', //color for bar
            data: [
                {
                    progress: ".Json::htmlEncode([$questions['question2'][1]]).",
                    labelText: 'Да ,день и время соотв. указаниям в доставке'
                },
                {
                    progress: ".Json::htmlEncode([$questions['question2'][2]]).",
                    labelText: 'Нет, приехал в назнач. день, но в другое аремя'
                },
                {
                    progress: ".Json::htmlEncode([$questions['question2'][3]]).",
                    labelText: 'Нет, приехал в другой день.'
                },
            ]
        }
    );
    barChartData.push(
        {
            element: '.bar-chart-3', // class for element
            color: '#8ECEF3', //color for bar
            data: [
                {
                    progress: ".Json::htmlEncode([$questions['question3'][1]]).",
                    labelText: 'Все хорошо'
                },
                {
                    progress: ".Json::htmlEncode([$questions['question3'][2]]).",
                    labelText: 'Не расспаковывал(а) /Покупал(а) не себе'
                },
                {
                    progress: ".Json::htmlEncode([$questions['question3'][3]]).",
                    labelText: 'Упаковка в пыли /испачкана'
                },
                {
                    progress: ".Json::htmlEncode([$questions['question3'][4]]).",
                    labelText: 'Упаковка помята'
                },
            ]
        }
    );
    barChartData.push(
        {
            element: '.bar-chart-4', // class for element
            color: '#83D1CB', //color for bar
            data: [
                {
                    progress: ".Json::htmlEncode([$questions['question4'][1]]).",
                    labelText: 'Позвонил не менее чем за час'
                },
                {
                    progress: ".Json::htmlEncode([$questions['question4'][2]]).",
                    labelText: 'Курьер позвонил только по приезде'
                },
                {
                    progress: ".Json::htmlEncode([$questions['question4'][3]]).",
                    labelText: 'Курьер не предупредил о приезде'
                },
            ]
        }
    );
    ",
    View::POS_HEAD,
    'barChartData'
);
?>

<div class="container">
    <div class="header">
        <div class="navbar-aside">
            <div class="mobile-menu btn-aside-menu toggle-sidebar">
                <div class="b-menu">
                    <div class="b-bun b-bun--top"></div>
                    <div class="b-bun b-bun--mid"></div>
                    <div class="b-bun b-bun--bottom"></div>
                </div>
            </div>
        </div>
    </div>
    <?= $this->render('/site/sidebar', [
        'id' => $id,
    ]) ?>
    <div class="content" id="content">
        <div class="page-content bar-charts-content">
            <?= SurveyFilterWidget::widget([
                'title' => 'Опрос по доставке / Ключевые показатели',
                'filters' => $filters,
            ]) ?>
            <div class="box-wrapper nps-row">
                <h3>
                    <span class="bold">NPS</span> Какая вероятность того, что вы порекомендуете друзьям и знакомым
                    совершить покупку техники в магазине
                </h3>
                <div class="main-nps">
                    <div>
                        <table class="block-table table-striped" id="faceectable">
                            <tbody>
                            <tr>
                                <td class="cntr_num" colspan="7">
                                    DETRACTORS
                                </td>
                                <td class="cntr_num" colspan="2">
                                    PASSIVES
                                </td>
                                <td class="cntr_num" colspan="2">
                                    PROMOTERS
                                </td>
                            </tr>
                            <tr>
                                <td class="cntr_num" colspan="7">
                                    <div class="planka-long"></div>
                                </td>
                                <td class="cntr_num" colspan="2">
                                    <div class="planka-long"></div>
                                </td>
                                <td class="cntr_num" colspan="2">
                                    <div class="planka-long"></div>
                                </td>
                            </tr>
                            <tr class="rate-th">
                                <?php foreach ($nps['grades'] as $key => $value): ?>
                                    <td>
                                        <div class="rate-item"><?= $key ?></div>
                                    </td>
                                <?php endforeach; ?>
                            </tr>
                            <tr class="dig-td">
                                <?php foreach ($nps['grades'] as $key => $value): ?>
                                    <td>
                                        <p class="sum-bot"><?= $value ?></p>
                                    </td>
                                <?php endforeach; ?>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="left-block">
                        <div class="promot-wrap">
                            <div class="label-promot">
                                <div class="table-txt11-sp"><?= $nps['promoters'][0] ?></div>
                                <div class="table-txt12-sp">Promoters</div>
                                <div class="table-txt13-sp"><?= $nps['promoters'][1] ?>%</div>
                            </div>
                            <div class="label-promot">
                                <div class="table-txt11-sp"><?= $nps['passives'][0] ?></div>
                                <div class="table-txt12-sp">Passives</div>
                                <div class="table-txt13-sp"><?= $nps['passives'][1] ?>%</div>
                            </div>
                            <div class="label-promot">
                                <div class="table-txt11-sp"><?= $nps['detractors'][0] ?></div>
                                <div class="table-txt12-sp">Detractors</div>
                                <div class="table-txt13-sp"><?= $nps['detractors'][1] ?>%</div>
                            </div>
                        </div>
                        <div class="block-circle">
                            <div class="circle">
                                <span>Total</span>
                                <span><?= $nps['total'] ?></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-row">
                <div class="box-wrapper bar-wrapper">
                    <h3>Как вы оцениваете доставку товара?</h3>
                    <div class="bar-chart-wrapper">
                        <div class="bar-chart-1 bar-chart-box">
                        </div>
                    </div>
                </div>
                <div class="box-wrapper">
                    <h3>Курьер приехал в согласованное время доставки?</h3>
                    <div class="bar-chart-wrapper">
                        <div class="bar-chart-2 bar-chart-box">
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-row">
                <div class="box-wrapper bar-wrapper">
                    <h3>Как вы оцениваете сохранность упаковки самого товара?</h3>
                    <div class="bar-chart-wrapper">
                        <div class="bar-chart-3 bar-chart-box">
                        </div>
                    </div>
                </div>
                <div class="box-wrapper">
                    <h3>Курьер предупредил ли заранее о своем приезде?</h3>
                    <div class="bar-chart-wrapper">
                        <div class="bar-chart-4 bar-chart-box">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
