<?php

use yii\helpers\Url;
use yii\helpers\Html;

?>

<div class="container">
    <div class="header">
        <div class="navbar-aside">
            <div class="mobile-menu btn-aside-menu toggle-sidebar">
                <div class="b-menu">
                    <div class="b-bun b-bun--top"></div>
                    <div class="b-bun b-bun--mid"></div>
                    <div class="b-bun b-bun--bottom"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="sidebar">
        <div class="menu-sidebar">
            <div class="logo">
                <a href="<?= Url::home() ?>">
                    <?= Html::img('@web/img/logo.svg', ['alt' => 'reStore']) ?>
                </a>
            </div>
            <div class="menu">
                <nav>
                    <ul class="sidebar-nav">
                        <li>
                            <a href="<?= Url::home() ?>">
                                <div class="icon icon-chart"></div>
                                Ключевые показатели
                            </a>
                        </li>
                        <li class="active">
                            <a href="<?= Url::to(['/surveys']) ?>">
                                <div class="icon icon-file"></div>
                                Все анкеты
                            </a>
                        </li>
                        <li>
                            <a href="<?= Url::to(['/users']) ?>">
                                <div class="icon icon-users"></div>
                                Пользователи
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <div class="content" id="content">
        <div class="page-content">
            <div class="pink-block">
                <div class="page-header">
                    Все анкеты
                </div>
                <form action="#" method="get" data-pjax="1">
                    <div class="filter-wrapper">
                        <!-- блок выбора даты -->
                        <div class="box box-date">
                            <h6>Период</h6>
                            <div class="form-group">
                                <input type="text" class="form-control" value="13-04-2021 - 13-05-2021">
                                <div class="help-block"></div>
                            </div>
                        </div>

                        <!-- блок выбора расположения -->
                        <div class="box">
                            <h6>Магазин</h6>
                            <div class="form-group">
                                <select class="form-control">
                                    <option value="">Все</option>
                                    <option value="1">Магазин 1</option>
                                    <option value="2">Магазин 2</option>
                                </select>

                                <div class="help-block"></div>
                            </div>
                        </div>
                        <!-- блок выбора расположения -->
                        <div class="box">
                            <h6>Отдел </h6>
                            <div class="form-group">
                                <select class="form-control">
                                    <option value="">Все</option>
                                    <option value="1">Отдел 1</option>
                                    <option value="2">Отдел 2</option>
                                </select>

                                <div class="help-block"></div>
                            </div>
                        </div>
                        <!-- блок выбора расположения -->
                        <div class="box">
                            <h6>Опросы</h6>
                            <div class="form-group">
                                <select class="form-control">
                                    <option value="">Все</option>
                                    <option value="1">Собственное производство</option>
                                    <option value="2">Касса</option>
                                    <option value="3">Фрукты и овощи</option>
                                    <option value="4">Сервисная стойка</option>
                                    <option value="5">Покупательские шкафчики</option>
                                </select>

                                <div class="help-block"></div>
                            </div>
                        </div>
                        <input type="hidden" value=""/>
                    </div>
                </form>
                <div class="alerts-wrap">
                    <div class="alert-item">
                        <a href="#" class="alert-row">
                            <div class="info-col">
                                <div class="name">
                                    Всего анкет
                                </div>
                                <div class="sum">
                                    9997
                                </div>
                            </div>
                            <div class="icon-col">
                                <div class="icon icon-all-anket"></div>
                            </div>
                        </a>
                    </div>
                    <div class="alert-item">
                        <a href="#" class="alert-row">
                            <div class="info-col">
                                <div class="name">
                                    Доля Алертов
                                </div>
                                <div class="sum">
                                    7,8 %
                                </div>
                            </div>
                            <div class="icon-col">
                                <div class="icon icon-share-alert"></div>
                            </div>
                        </a>
                    </div>
                    <div class="alert-item">
                        <a href="#" class="alert-row">
                            <div class="info-col">
                                <div class="name">
                                    Просроч. Алерты
                                </div>
                                <div class="sum">
                                    73
                                </div>
                            </div>
                            <div class="icon-col">
                                <div class="icon icon-expired-alert"></div>
                            </div>
                        </a>
                    </div>
                    <div class="alert-item">
                        <a href="#" class="alert-row">
                            <div class="info-col">
                                <div class="name">
                                    Новые Алерты
                                </div>
                                <div class="sum">
                                    30
                                </div>
                            </div>
                            <div class="icon-col">
                                <div class="icon icon-new-alert"></div>
                            </div>
                        </a>
                    </div>
                    <div class="alert-item">
                        <a href="#" class="alert-row">
                            <div class="info-col">
                                <div class="name">
                                    Алерты в работе
                                </div>
                                <div class="sum">
                                    49
                                </div>
                            </div>
                            <div class="icon-col">
                                <div class="icon icon-alert-in-work"></div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="search-row">
                <div class="search-wrap">
                    <form action="#">
                        <input type="text">
                        <button class="search-submit"></button>
                    </form>
                </div>
                <a href="#" class="unload-wrap">
                    <div class="icon">
                    </div>
                    <div class="text">
                        <div class="big-text">
                            Выгрузка
                        </div>
                        <div class="small-text">
                            Скачать файл
                        </div>
                    </div>
                </a>
            </div>
            <div class="table-wrap">
                <div class="table-responsive">
                    <table>
                        <thead>
                        <tr>
                            <th>
                                Индикатор<br>
                                Алерта
                            </th>
                            <th>
                                Статус<br>
                                Алерта
                            </th>
                            <th>
                                Номер <br>
                                Алерта
                            </th>
                            <th>
                                Дата <br>
                                опроса
                            </th>
                            <th>
                                Оценка<br>
                            </th>
                            <th>
                                Магазин
                            </th>
                            <th>
                                Отдел
                            </th>
                            <th>
                                Опрос
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <a href="one-anket">-</a>
                            </td>
                            <td>

                            </td>
                            <td>
                                125363
                            </td>
                            <td>
                                12.11.2021 12:43
                            </td>
                            <td>
                                5
                            </td>
                            <td>
                                г Москва, 1-й Котляковский
                            </td>
                            <td>
                                Отдел 2
                            </td>
                            <td>
                                Собственное производство
                            </td>
                        </tr>
                        <tr class="alert-row">
                            <td>
                                <a href="one-anket"><div class="alert-label">!ALERT</div></a>
                            </td>
                            <td>
                                Новый
                            </td>
                            <td>
                                125363
                            </td>
                            <td>
                                12.11.2021 12:43
                            </td>
                            <td>
                                1
                            </td>
                            <td>
                                г Москва, 1-й Котляковский
                            </td>
                            <td>
                                Отдел 2
                            </td>
                            <td>
                                Покупательские шкафчики
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a href="one-anket">-</a>
                            </td>
                            <td>

                            </td>
                            <td>
                                125363
                            </td>
                            <td>
                                12.11.2021 12:43
                            </td>
                            <td>
                                5
                            </td>
                            <td>
                                г Москва, 1-й Котляковский
                            </td>
                            <td>
                                Отдел 2
                            </td>
                            <td>
                                Фрукты и овощи
                            </td>
                        </tr>
                        <tr class="alert-row">
                            <td>
                                <a href="one-anket"><div class="alert-label">!ALERT</div></a>
                            </td>
                            <td>
                                Новый
                            </td>
                            <td>
                                125363
                            </td>
                            <td>
                                12.11.2021 12:43
                            </td>
                            <td>
                                2
                            </td>
                            <td>
                                г Москва, 1-й Котляковский
                            </td>
                            <td>
                                Отдел 2
                            </td>
                            <td>
                                Касса
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a href="one-anket">-</a>
                            </td>
                            <td>

                            </td>
                            <td>
                                125363
                            </td>
                            <td>
                                12.11.2021 12:43
                            </td>
                            <td>
                                4
                            </td>
                            <td>
                                г Москва, ул 8-я Текстильщиков,
                            </td>
                            <td>
                                Отдел 2
                            </td>
                            <td>
                                Сервисная стойка
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="pagination-wrap">
                <ul class="pagination">
                    <li class="prev disabled"><span>Назад</span></li>
                    <li class="active"><a href="#" data-page="0">1</a></li>
                    <li><a href="#" data-page="1">2</a></li>
                    <li><a href="#" data-page="2">3</a></li>
                    <li><a href="#" data-page="3">4</a></li>
                    <li class="next"><a href="#" data-page="1">Далее</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>