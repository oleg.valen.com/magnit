<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\models\User;

?>

<div class="sidebar">
    <div class="menu-sidebar">
        <div class="logo">
            <a href="<?= Url::home() ?>">
                <?= Html::img('@web/img/logo.svg', ['alt' => 'Magnit']) ?>
            </a>
        </div>
        <div class="menu">
            <nav>
                <ul class="sidebar-nav">
                    <li class="<?= $id == 'site' ? 'active' : '' ?>">
                        <a href="<?= Url::home() ?>">
                            <div class="icon icon-chart"></div>
                            Ключевые показатели
                        </a>
                    </li>
                    <li class="<?= $id == 'survey' ? 'active' : '' ?>">
                        <a href="<?= Url::to(['/surveys']) ?>">
                            <div class="icon icon-file"></div>
                            Все анкеты
                        </a>
                    </li>
                    <?php if (Yii::$app->user->can('viewUser_')): ?>
                    <?php endif; ?>
                    <li class="<?= $id == 'user' ? 'active' : '' ?>">
                        <a href="<?= Url::to(['/users']) ?>">
                            <div class="icon icon-users"></div>
                            Пользователи
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</div>
