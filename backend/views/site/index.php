<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\web\View;
use backend\views\widgets\SiteFilterWidget;
use common\models\SurveyType;
use yii\helpers\Json;

$this->registerJs(
    "//data for shart
     let barChartData = [
        {
            element: '.chart-4', // class for element
            color: '#8ECEF3', //color for bar
            data: [
                {
                    progress: " . Json::htmlEncode($questions[SurveyType::CASH]['q']['question1'][1]) . ",
                    labelText: '1'
                },
                {
                    progress: " . Json::htmlEncode($questions[SurveyType::CASH]['q']['question1'][2]) . ",
                    labelText: '2'
                },
                {
                    progress: " . Json::htmlEncode($questions[SurveyType::CASH]['q']['question1'][3]) . ",
                    labelText: '3'
                },
                {
                    progress: " . Json::htmlEncode($questions[SurveyType::CASH]['q']['question1'][4]) . ",
                    labelText: '4'
                },
                {
                    progress: " . Json::htmlEncode($questions[SurveyType::CASH]['q']['question1'][5]) . ",
                    labelText: '5'
                },
            ]
        },
        {
            element: '.chart-5', // class for element
            color: '#ECCCFF', //color for bar
            data: [
                {
                    progress: " . Json::htmlEncode($questions[SurveyType::CASH]['q']['question2'][1]) . ",
                    labelText: '1'
                },
                {
                    progress: " . Json::htmlEncode($questions[SurveyType::CASH]['q']['question2'][2]) . ",
                    labelText: '2'
                },
                {
                    progress: " . Json::htmlEncode($questions[SurveyType::CASH]['q']['question2'][3]) . ",
                    labelText: '3'
                },
                {
                    progress: " . Json::htmlEncode($questions[SurveyType::CASH]['q']['question2'][4]) . ",
                    labelText: '4'
                },
                {
                    progress: " . Json::htmlEncode($questions[SurveyType::CASH]['q']['question2'][5]) . ",
                    labelText: '5'
                },
            ]
        }
    ];
    let chartData = [
        {
            element: '.chart-1', // class for element
            type: 'lineDot',               //type of chart
            dotColor: '#FFD1E2',
            data: [                          //data
                {
                    backgroundStart: '#FD2679',
                    backgroundEnd: '#FFD1E2',
                    progress: " . Json::htmlEncode($questions[SurveyType::PROD]['q']['question1'][1]) . ",
                    labelText: '1'
                },
                {
                    backgroundStart: '#FD2679',
                    backgroundEnd: '#FFD1E2',
                    progress: " . Json::htmlEncode($questions[SurveyType::PROD]['q']['question1'][2]) . ",
                    labelText: '2'
                },
                {
                    backgroundStart: '#FD2679',
                    backgroundEnd: '#FFD1E2',
                    progress: " . Json::htmlEncode($questions[SurveyType::PROD]['q']['question1'][3]) . ",
                    labelText: '3'
                },
                {
                    backgroundStart: '#FD2679',
                    backgroundEnd: '#FFD1E2',
                    progress: " . Json::htmlEncode($questions[SurveyType::PROD]['q']['question1'][4]) . ",
                    labelText: '4'
                },
                {
                    backgroundStart: '#FD2679',
                    backgroundEnd: '#FFD1E2',
                    progress: " . Json::htmlEncode($questions[SurveyType::PROD]['q']['question1'][5]) . ",
                    labelText: '5'
                }
            ],
        },
        {
            element: '.chart-2', // class for element
            type: 'lineDot',               //type of chart
            dotColor: '#F3A6FF',
            data: [                          //data
                {
                    backgroundStart: '#CC6ADC',
                    backgroundEnd: '#E9C5EF ',
                    progress: " . Json::htmlEncode($questions[SurveyType::PROD]['q']['question2'][1]) . ",
                    labelText: '1'
                },
                {
                    backgroundStart: '#BA7BC4',
                    backgroundEnd: '#CFBED2',
                    progress: " . Json::htmlEncode($questions[SurveyType::PROD]['q']['question2'][2]) . ",
                    labelText: '2'
                },
                {
                    backgroundStart: '#DF9EE9',
                    backgroundEnd: '#E8DCE1',
                    progress: " . Json::htmlEncode($questions[SurveyType::PROD]['q']['question2'][3]) . ",
                    labelText: '3'
                },
                {
                    backgroundStart: '#E7C2EB',
                    backgroundEnd: '#FCEAFF',
                    progress: " . Json::htmlEncode($questions[SurveyType::PROD]['q']['question2'][4]) . ",
                    labelText: '4'
                },
                {
                    backgroundStart: '#E7C2EB',
                    backgroundEnd: '#FCEAFF',
                    progress: " . Json::htmlEncode($questions[SurveyType::PROD]['q']['question2'][5]) . ",
                    labelText: '5'
                }
            ],
        },
        {
            element: '.chart-3', // class for element
            type: 'lineDot',               //type of chart
            dotColor: '#A8A6F2',
            data: [                          //data
                {
                    backgroundStart: '#413DFF',
                    backgroundEnd: '#ADACFF ',
                    progress: " . Json::htmlEncode($questions[SurveyType::PROD]['q']['question3'][1]) . ",
                    labelText: '1'
                },
                {
                    backgroundStart: '#5450F1',
                    backgroundEnd: '#918FED',
                    progress: " . Json::htmlEncode($questions[SurveyType::PROD]['q']['question3'][2]) . ",
                    labelText: '2'
                },
                {
                    backgroundStart: '#6E6BEF',
                    backgroundEnd: '#A9A7F2',
                    progress: " . Json::htmlEncode($questions[SurveyType::PROD]['q']['question3'][3]) . ",
                    labelText: '3'
                },
                {
                    backgroundStart: '#8F8DF3',
                    backgroundEnd: '#BDBBFF',
                    progress: " . Json::htmlEncode($questions[SurveyType::PROD]['q']['question3'][4]) . ",
                    labelText: '4'
                },
                {
                    backgroundStart: '#A6A3FD',
                    backgroundEnd: '#E6E5FF',
                    progress: " . Json::htmlEncode($questions[SurveyType::PROD]['q']['question3'][5]) . ",
                    labelText: '5'
                }
            ],
        },
        {
            element: '.chart-6', // class for element
            type: 'horizontalbar',               //тип графіка
            data: [                          //данные графика
                {
                    background: '#F3D0FF',
                    progress: " . Json::htmlEncode($questions[SurveyType::FRUIT]['q']['question1'][5]) . ",
                    labelText: '1'
                },
                {
                    background: '#78F3EF',
                    progress: " . Json::htmlEncode($questions[SurveyType::FRUIT]['q']['question1'][4]) . ",
                    labelText: '2'
                },
                {
                    background: '#FFEDAB',
                    progress: " . Json::htmlEncode($questions[SurveyType::FRUIT]['q']['question1'][3]) . ",
                    labelText: '3'
                },
                {
                    background: '#00D2FB',
                    progress: " . Json::htmlEncode($questions[SurveyType::FRUIT]['q']['question1'][2]) . ",
                    labelText: '4'
                },
                {
                    background: '#FFA2C6',
                    progress: " . Json::htmlEncode($questions[SurveyType::FRUIT]['q']['question1'][1]) . ",
                    labelText: '5'
                }
            ],
        },
        {
            element: '.chart-7', // class for element
            type: 'horizontalbar',               //тип графіка
            data: [                          //данные графика
                {
                    background: '#F69394',
                    progress: " . Json::htmlEncode($questions[SurveyType::FRUIT]['q']['question2'][5]) . ",
                    labelText: '1'
                },
                {
                    background: '#FFF386',
                    progress: " . Json::htmlEncode($questions[SurveyType::FRUIT]['q']['question2'][4]) . ",
                    labelText: '2'
                },
                {
                    background: '#FADA99',
                    progress: " . Json::htmlEncode($questions[SurveyType::FRUIT]['q']['question2'][3]) . ",
                    labelText: '3'
                },
                {
                    background: '#A9DDD6',
                    progress: " . Json::htmlEncode($questions[SurveyType::FRUIT]['q']['question2'][2]) . ",
                    labelText: '4'
                },
                {
                    background: '#8ED995',
                    progress: " . Json::htmlEncode($questions[SurveyType::FRUIT]['q']['question2'][1]) . ",
                    labelText: '5'
                }
            ]
        },
        {
            element: '.chart-8', // class for element
            type: 'pieRound',               //тип графіка
            innerText: 'CSI',
            innerValue: 8.75,
            data: [                          //данные графика
                {
                    background: '#B2FDBA',
                    gradient: ['#B3FFBB', '#ABEFB2'],
                    progress: " . Json::htmlEncode($questions[SurveyType::SERVICE]['q']['question1'][1]) . ",
                    labelText: 'Да'
                },
                {
                    background: '#FEC1C1',
                    gradient: ['#FEC4C4', '#FFAAAA'],
                    progress: " . Json::htmlEncode($questions[SurveyType::SERVICE]['q']['question1'][2]) . ",
                    labelText: 'Нет'
                },
            ]
        },
        {
            element: '.chart-9', // class for element
            type: 'pieRound',               //тип графіка
            innerText: 'CSI',
            innerValue: 8.75,
            data: [                          //данные графика
                {
                    background: '#B2FDBA',
                    gradient: ['#B3FFBB', '#ABEFB2'],
                    progress: " . Json::htmlEncode($questions[SurveyType::SERVICE]['q']['question2'][1]) . ",
                    labelText: 'Да'
                },
                {
                    background: '#FEC1C1',
                    gradient: ['#FEC4C4', '#FFAAAA'],
                    progress: " . Json::htmlEncode($questions[SurveyType::SERVICE]['q']['question2'][2]) . ",
                    labelText: 'Нет'
                },
            ]
        },
        {
            element: '.chart-10', // class for element
            type: 'shadowLine',               //тип графіка
            borderColor: '#BDA0FF',
            data: [                          //данные графика
                {
                    progress: " . Json::htmlEncode($questions[SurveyType::SHOPPING]['q']['question1'][1]) . ",
                    labelText: '1'
                },
                {
                    progress: " . Json::htmlEncode($questions[SurveyType::SHOPPING]['q']['question1'][2]) . ",
                    labelText: '2'
                },
                {
                    progress: " . Json::htmlEncode($questions[SurveyType::SHOPPING]['q']['question1'][3]) . ",
                    labelText: '3'
                },
                {
                    progress: " . Json::htmlEncode($questions[SurveyType::SHOPPING]['q']['question1'][4]) . ",
                    labelText: '4'
                },
                {
                    progress: " . Json::htmlEncode($questions[SurveyType::SHOPPING]['q']['question1'][5]) . ",
                    labelText: '5'
                }
            ]
        },
        {
            element: '.chart-11', // class for element
            type: 'shadowLine',               //тип графіка
            borderColor: '#2D99FF',
            data: [                          //данные графика
                {
                    progress: " . Json::htmlEncode($questions[SurveyType::SHOPPING]['q']['question2'][1]) . ",
                    labelText: '1'
                },
                {
                    progress: " . Json::htmlEncode($questions[SurveyType::SHOPPING]['q']['question2'][2]) . ",
                    labelText: '2'
                },
                {
                    progress: " . Json::htmlEncode($questions[SurveyType::SHOPPING]['q']['question2'][3]) . ",
                    labelText: '3'
                },
                {
                    progress: " . Json::htmlEncode($questions[SurveyType::SHOPPING]['q']['question2'][4]) . ",
                    labelText: '4'
                },
                {
                    progress: " . Json::htmlEncode($questions[SurveyType::SHOPPING]['q']['question2'][5]) . ",
                    labelText: '5'
                }
            ]
        },
        {
            element: '.chart-12', // class for element
            type: 'shadowLine',               //тип графіка
            borderColor: '#16CEB9',
            data: [                          //данные графика
                {
                    progress: " . Json::htmlEncode($questions[SurveyType::SHOPPING]['q']['question3'][1]) . ",
                    labelText: '1'
                },
                {
                    progress: " . Json::htmlEncode($questions[SurveyType::SHOPPING]['q']['question3'][2]) . ",
                    labelText: '2'
                },
                {
                    progress: " . Json::htmlEncode($questions[SurveyType::SHOPPING]['q']['question3'][3]) . ",
                    labelText: '3'
                },
                {
                    progress: " . Json::htmlEncode($questions[SurveyType::SHOPPING]['q']['question3'][4]) . ",
                    labelText: '4'
                },
                {
                    progress: " . Json::htmlEncode($questions[SurveyType::SHOPPING]['q']['question3'][5]) . ",
                    labelText: '5'
                }
            ]
        },
    ];",
    View::POS_HEAD
);
?>

<div class="container">
    <div class="header">
        <div class="navbar-aside">
            <div class="mobile-menu btn-aside-menu toggle-sidebar">
                <div class="b-menu">
                    <div class="b-bun b-bun--top"></div>
                    <div class="b-bun b-bun--mid"></div>
                    <div class="b-bun b-bun--bottom"></div>
                </div>
            </div>
        </div>
    </div>
    <?= $this->render('/site/sidebar', [
        'id' => $id,
    ]) ?>
    <div class="content" id="content">
        <div class="page-content">
            <?= SiteFilterWidget::widget([
                'title' => 'Ключевые показатели',
                'filters' => $filters,
            ]) ?>
            <div class="tabs-link-wrap">
                <div class="tab-link" data-link="chart-tab-1">
                    <div class="tab-text">Собственное производство</div>
                </div>
                <div class="tab-link" data-link="chart-tab-2">
                    <div class="tab-text">Касса</div>
                </div>
                <div class="tab-link" data-link="chart-tab-3">
                    <div class="tab-text">Фрукты и овощи</div>
                </div>
                <div class="tab-link" data-link="chart-tab-4">
                    <div class="tab-text">Сервисная стойка</div>
                </div>
                <div class="tab-link" data-link="chart-tab-5">
                    <div class="tab-text">Покупательские шкафчики</div>
                </div>
            </div>
            <div class="tabs-cont-wrap">
                <div class="tab-content chart-tab-1">
                    <div class="box-row">
                        <div class="box-wrapper box-chart">
                            <h3>
                                1. Оцените качество товаров в отделе Собственного производства
                            </h3>
                            <div class="chart-content">
                                <div class="lineDot">
                                    <div class="chart">
                                        <div class="chart-1">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-wrapper box-chart">
                            <h3>
                                2. Оцените ассортимент товаров в отделе Собственного производства
                            </h3>
                            <div class="chart-content">
                                <div class="lineDot">
                                    <div class="chart">
                                        <div class="chart-2">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-row">
                        <div class="box-wrapper box-chart">
                            <h3>
                                3. Оцените доброжелательность продавца в отделе Собственного производства
                            </h3>
                            <div class="chart-content">
                                <div class="lineDot">
                                    <div class="chart">
                                        <div class="chart-3">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-wrapper">
                            <h3>
                                4. Если у Вас есть предложения, просьба оставить их здесь
                            </h3>
                            <div class="chart-content">
                                <div class="comment-chart">
                                    <div class="info-col">
                                        <div class="label">
                                            Количество комментариев
                                        </div>
                                        <div class="value">
                                            <?= $questions[SurveyType::PROD]['totalComment'] ?>
                                        </div>
                                    </div>
                                    <div class="progress-wrap">
                                        <div class="percent progress-cirlce" data-percent="<?= $questions[SurveyType::PROD]['total'] != 0 ? round($questions[SurveyType::PROD]['totalComment'] / $questions[SurveyType::PROD]['total'] * 100) : 0 ?>">
                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="-1 -1 34 34">
                                                <circle cx="16" cy="16" r="15.9155"
                                                        class="progress-bar__background"/>
                                                <circle cx="16" cy="16" r="15.9155"
                                                        class="progress-bar__progress js-progress-bar"/>
                                            </svg>
                                        </div>
                                        <div class="text">
                                            <?= $questions[SurveyType::PROD]['total'] != 0 ? round($questions[SurveyType::PROD]['totalComment'] / $questions[SurveyType::PROD]['total'] * 100) : 0 ?>
                                            %
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-content chart-tab-2">
                    <div class="box-row">
                        <div class="box-wrapper box-chart">
                            <h3>
                                1. Оцените доброжелательность кассира
                            </h3>
                            <div class="bar-chart-wrapper">
                                <div class="chart-4 bar-chart-box">
                                </div>
                            </div>
                        </div>
                        <div class="box-wrapper box-chart">
                            <h3>
                                2. Оцените скорость облуживания на кассе
                            </h3>
                            <div class="bar-chart-wrapper">
                                <div class="chart-5 bar-chart-box">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-row">
                        <div class="box-wrapper">
                            <h3>
                                3. Если у Вас есть предложения, просьба оставить их здесь
                            </h3>
                            <div class="chart-content">
                                <div class="comment-chart">
                                    <div class="info-col">
                                        <div class="label">
                                            Количество комментариев
                                        </div>
                                        <div class="value">
                                            <?= $questions[SurveyType::CASH]['totalComment'] ?>
                                        </div>
                                    </div>
                                    <div class="progress-wrap">
                                        <div class="percent progress-cirlce" data-percent="<?= $questions[SurveyType::CASH]['total'] != 0 ? round($questions[SurveyType::CASH]['totalComment'] / $questions[SurveyType::CASH]['total'] * 100) : 0 ?>">
                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="-1 -1 34 34">
                                                <circle cx="16" cy="16" r="15.9155"
                                                        class="progress-bar__background"/>
                                                <circle cx="16" cy="16" r="15.9155"
                                                        class="progress-bar__progress js-progress-bar"/>
                                            </svg>
                                        </div>
                                        <div class="text">
                                            <?= $questions[SurveyType::CASH]['total'] != 0 ? round($questions[SurveyType::CASH]['totalComment'] / $questions[SurveyType::CASH]['total'] * 100) : 0 ?>
                                            %
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-content chart-tab-3">
                    <div class="box-row">
                        <div class="box-wrapper box-chart">
                            <h3>
                                1. Оцените свежесть товаров в Отделе Фрукты и овощи
                            </h3>
                            <div class="chart-content">
                                <div class="horizonatlbar">
                                    <div class="chart">
                                        <div class="chart-6"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-wrapper box-chart">
                            <h3>
                                2. Оцените чистоту в Отделе Фрукты и овощи
                            </h3>
                            <div class="chart-content">
                                <div class="horizonatlbar">
                                    <div class="chart">
                                        <div class="chart-7"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-row">
                        <div class="box-wrapper">
                            <h3>
                                3. Если у Вас есть предложения, просьба оставить их здесь
                            </h3>
                            <div class="chart-content">
                                <div class="comment-chart">
                                    <div class="info-col">
                                        <div class="label">
                                            Количество комментариев
                                        </div>
                                        <div class="value">
                                            <?= $questions[SurveyType::FRUIT]['totalComment'] ?>
                                        </div>
                                    </div>
                                    <div class="progress-wrap">
                                        <div class="percent progress-cirlce" data-percent="<?= $questions[SurveyType::FRUIT]['total'] != 0 ? round($questions[SurveyType::FRUIT]['totalComment'] / $questions[SurveyType::FRUIT]['total'] * 100) : 0 ?>">
                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="-1 -1 34 34">
                                                <circle cx="16" cy="16" r="15.9155"
                                                        class="progress-bar__background"/>
                                                <circle cx="16" cy="16" r="15.9155"
                                                        class="progress-bar__progress js-progress-bar"/>
                                            </svg>
                                        </div>
                                        <div class="text">
                                            <?= $questions[SurveyType::FRUIT]['total'] != 0 ? round($questions[SurveyType::FRUIT]['totalComment'] / $questions[SurveyType::FRUIT]['total'] * 100) : 0 ?>
                                            %
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-content chart-tab-4">
                    <div class="box-row">
                        <div class="box-wrapper box-chart">
                            <h3>
                                1. Решили ли сотрудники Ваш вопрос?
                            </h3>
                            <div class="chart-content">
                                <div class="pieRound smalllegend">
                                    <div class="chart">
                                        <div class="chart-8">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-wrapper box-chart">
                            <h3>
                                2. Стремились ли сотрудники помочь Вам?
                            </h3>
                            <div class="chart-content">
                                <div class="pieRound smalllegend">
                                    <div class="chart">
                                        <div class="chart-9">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-row">
                        <div class="box-wrapper">
                            <h3>
                                3. Если у Вас есть предложения, просьба оставить их здесь
                            </h3>
                            <div class="chart-content">
                                <div class="comment-chart">
                                    <div class="info-col">
                                        <div class="label">
                                            Количество комментариев
                                        </div>
                                        <div class="value">
                                            <?= $questions[SurveyType::SERVICE]['totalComment'] ?>
                                        </div>
                                    </div>
                                    <div class="progress-wrap">
                                        <div class="percent progress-cirlce" data-percent="<?= $questions[SurveyType::SERVICE]['total'] != 0 ? round($questions[SurveyType::SERVICE]['totalComment'] / $questions[SurveyType::SERVICE]['total'] * 100) : 0 ?>">
                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="-1 -1 34 34">
                                                <circle cx="16" cy="16" r="15.9155"
                                                        class="progress-bar__background"/>
                                                <circle cx="16" cy="16" r="15.9155"
                                                        class="progress-bar__progress js-progress-bar"/>
                                            </svg>
                                        </div>
                                        <div class="text">
                                            <?= $questions[SurveyType::SERVICE]['total'] != 0 ? round($questions[SurveyType::SERVICE]['totalComment'] / $questions[SurveyType::SERVICE]['total'] * 100) : 0 ?>
                                            %
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-content chart-tab-5">
                    <div class="box-row">
                        <div class="box-wrapper box-chart">
                            <h3>
                                1. Оцените дружелюбность и отзывчивость персонала
                            </h3>
                            <div class="chart-content">
                                <div class="lineChart">
                                    <div class="chart">
                                        <div class="chart-10">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-wrapper box-chart">
                            <h3>
                                2. Оцените ассортимент в магазине
                            </h3>
                            <div class="chart-content">
                                <div class="lineChart">
                                    <div class="chart">
                                        <div class="chart-11">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-row">
                        <div class="box-wrapper box-chart">
                            <h3>
                                3. Насколько Вы готовы рекомендовать данный магазин?
                            </h3>
                            <div class="chart-content">
                                <div class="lineChart">
                                    <div class="chart">
                                        <div class="chart-12">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-wrapper">
                            <h3>
                                4. Если у Вас есть предложения, просьба оставить их здесь
                            </h3>
                            <div class="chart-content">
                                <div class="comment-chart">
                                    <div class="info-col">
                                        <div class="label">
                                            Количество комментариев
                                        </div>
                                        <div class="value">
                                            <?= $questions[SurveyType::SHOPPING]['totalComment'] ?>
                                        </div>
                                    </div>
                                    <div class="progress-wrap">
                                        <div class="percent progress-cirlce" data-percent="<?= $questions[SurveyType::SHOPPING]['total'] != 0 ? round($questions[SurveyType::SHOPPING]['totalComment'] / $questions[SurveyType::SHOPPING]['total'] * 100) : 0 ?>">
                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="-1 -1 34 34">
                                                <circle cx="16" cy="16" r="15.9155"
                                                        class="progress-bar__background"/>
                                                <circle cx="16" cy="16" r="15.9155"
                                                        class="progress-bar__progress js-progress-bar"/>
                                            </svg>
                                        </div>
                                        <div class="text">
                                            <?= $questions[SurveyType::SHOPPING]['total'] != 0 ? round($questions[SurveyType::SHOPPING]['totalComment'] / $questions[SurveyType::SHOPPING]['total'] * 100) : 0 ?>
                                            %
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
