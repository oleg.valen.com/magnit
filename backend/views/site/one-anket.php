<?php

use yii\helpers\Url;
use yii\helpers\Html;

?>

<div class="container">
    <div class="header">
        <div class="navbar-aside">
            <div class="mobile-menu btn-aside-menu toggle-sidebar">
                <div class="b-menu">
                    <div class="b-bun b-bun--top"></div>
                    <div class="b-bun b-bun--mid"></div>
                    <div class="b-bun b-bun--bottom"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="sidebar">
        <div class="menu-sidebar">
            <div class="logo">
                <a href="<?= Url::home() ?>">
                    <?= Html::img('@web/img/logo.svg', ['alt' => 'reStore']) ?>
                </a>
            </div>
            <div class="menu">
                <nav>
                    <ul class="sidebar-nav">
                        <li>
                            <a href="<?= Url::home() ?>">
                                <div class="icon icon-chart"></div>
                                Ключевые показатели
                            </a>
                        </li>
                        <li class="active">
                            <a href="<?= Url::to(['/surveys']) ?>">
                                <div class="icon icon-file"></div>
                                Все анкеты
                            </a>
                        </li>
                        <li>
                            <a href="<?= Url::to(['/users']) ?>">
                                <div class="icon icon-users"></div>
                                Пользователи
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <div class="content" id="content">
        <form action="" id="anket-form">
            <div class="page-content">
                <div class="one-anket-header">
                    <div class="pink-block profile-header">
                        <div class="top-nav">
                            <div class="navbar-aside">
                                <div class="mobile-menu btn-aside-menu toggle-sidebar">
                                    <div class="b-menu">
                                        <div class="b-bun b-bun--top"></div>
                                        <div class="b-bun b-bun--mid"></div>
                                        <div class="b-bun b-bun--bottom"></div>
                                        </div>
                                </div>
                            </div>
                            <div class="navbar-user">
                                <div class="user">
                                    <div class="user-wrapper show-user-dropdown">
                                        <div class="user-name">Admin</div>
                                        <div class="icon-user"></div>
                                    </div>
                                    <div class="dropdown-user">
                                        <ul>
                                            <li>
                                                <a href="#" class="logout">Выход </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="page-header">
                            Все анкеты / Профиль клиента
                        </div>
                        <div class="btn-back-wrap">
                            <a href="#" class="btn-back btn-default">Назад</a>
                        </div>
                        <div class="profile-wrapepr">
                            <div class="profile-photo-box">
                                <span class="photo-title">Профиль клиента</span>
                                <div class="photo-container">
                                    <img class="user-img" id="profileImage" src="./img/user.svg" alt="user">
                                    <div class="edit">
                                        <i class="edit-con"></i>
                                    </div>
                                    <input id="imageUpload" type="file" name="profile_photo" capture>
                                </div>
                            </div>
                            <div class="profile-info-box">
                                <div class="info-row">
                                    <div class="info-item">
                                        <p class="info-name">Номер Алерта</p>
                                        <p class="info-number">125363</p>
                                    </div>
                                    <div class="info-item">
                                        <p class="info-name">Дата опроса </p>
                                        <p class="info-number">12.11.2021 12:43</p>
                                    </div>
                                    <div class="info-item">
                                        <p class="info-name">Телефон</p>
                                        <p class="info-number">(861) 210-98-10</p>
                                    </div>
                                </div>
                                <div class="info-row">
                                    <div class="info-item">
                                        <p class="info-name">Магазин</p>
                                        <p class="info-number">Москва</p>
                                    </div>
                                    <div class="info-item">
                                        <p class="info-name">Отдел</p>
                                        <p class="info-number">Отдел 1</p>
                                    </div>
                                    <div class="info-item">
                                        <p class="info-name">E-mail</p>
                                        <p class="info-number">info@magnit.ru</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="client-answer-container">
                    <p class="answer-title">Ответы клиента</p>
                    <div class="answer-item">
                        <span class="q-text">1.	Оцените качество товаров в отделе Собственного производства</span>
                        <div class="answer-box">
                            <p class="answer-txt">2</p>
                        </div>
                    </div>
                    <div class="answer-item">
                        <span class="q-text">2.	Оцените ассортимент товаров в отделе Собственного производства</span>
                        <div class="answer-box">
                            <p class="answer-txt">4</p>
                        </div>
                    </div>
                    <div class="answer-item">
                        <span class="q-text">3.	Оцените доброжелательность продавца в отделе Собственного производства ?</span>
                        <div class="answer-box">
                            <p class="answer-txt">3</p>
                        </div>
                    </div>
                    <div class="answer-item">
                        <span class="q-text">4.	Если у Вас есть предложения, просьба оставить их здесь</span>
                        <div class="answer-box">
                            <p class="answer-txt">У меня нет предложений</p>
                        </div>
                    </div>
                    <div class="images-container">
                        <div class="img-item">
                            <img src="./img/login-bg-4.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="btn-wrapper take-to-work">
                    <button type="button" class="anket-btn">Взять в работу</button>
                </div>
                <div class="btn-wrapper complete-btn-wrapper">
                    <button type="button" class="anket-btn">Завершить выполнение</button>
                </div>
                <div class="comment-container">
                    <p>Уважаемая Татьяна!</p>
                    <textarea class="comment-field" name="" id=""></textarea>
                    <span class="comment-error">Поле обязательно к заполнению</span>
                    <div class="send-btn-wrp">
                        <button type="submit" class="send-btn">Отправить</button>
                    </div>
                </div>
                <div class="alert-history-container">
                    <p class="answer-title">История обработки Алерта</p>
                    <div class="history-item">
                        <p class="history-item-name">Время события:</p>
                        <p class="history-item-data">05-10-2021 12:00:32</p>
                    </div>
                    <div class="history-item">
                        <p class="history-item-name">ФИО:</p>
                        <p class="history-item-data">Синцова Татьяна</p>
                    </div>
                    <div class="history-item">
                        <p class="history-item-name">Действие:</p>
                        <p class="history-item-data">Взял в работу</p>
                    </div>
                    <div class="history-item">
                        <p class="history-item-name">Время взятия в работу:</p>
                        <p class="history-item-data">05-10-2021 12:00</p>
                    </div>
                    <div class="history-item">
                        <p class="history-item-name">Время закрытия Алерта:</p>
                        <p class="history-item-data">06-10-2021 15:30</p>
                    </div>
                    <div class="history-item">
                        <p class="history-item-name">ФИО:</p>
                        <p class="history-item-data">Синцова Татьяна</p>
                    </div>
                    <div class="history-item">
                        <p class="history-item-name">Действие:</p>
                        <p class="history-item-data">Закрыл Алерт</p>
                    </div>
                    <div class="history-item">
                        <p class="history-item-name">Комментарий о выполненой работе:</p>
                        <p class="history-item-data alert-comment">Спасибо, что приняли участие в опросе об уровне
                            нашего сервиса. Мы всегда стремимся к высокому уровню обслуживания клиентов и нам очень
                            жаль, что вы не увидели заинтересованности от наших сотрудников</p>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>