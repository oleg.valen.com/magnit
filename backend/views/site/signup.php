<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="login-page">
    <div class="login-box">
        <div class="login-box-body">
            <p class="login-box-header">Регистрация</p>
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Логин" aria-required="true" autocomplete
                       name="SignupForm[username]">
            </div>
            <div class="form-group">
                <input type="email" class="form-control" placeholder="E-mail" aria-required="true" autocomplete
                       name="SignupForm[email]">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" placeholder="Пароль" aria-required="true" autocomplete
                       name="SignupForm[password]">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" placeholder="Повторите пароль" aria-required="true" autocomplete
                       name="SignupForm[passwordRepeat]">
            </div>
            <div class="btn-wrap">
                <button type="submit" class="btn-submit" name="login-button">Зарегистрироваться</button>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>