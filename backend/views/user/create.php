<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\views\widgets\UserFilterWidget;
use yii\helpers\Url;
use common\models\User;

?>

<div class="container">
    <div class="header">
        <div class="navbar-aside">
            <div class="mobile-menu btn-aside-menu toggle-sidebar">
                <div class="b-menu">
                    <div class="b-bun b-bun--top"></div>
                    <div class="b-bun b-bun--mid"></div>
                    <div class="b-bun b-bun--bottom"></div>
                </div>
            </div>
        </div>
    </div>
    <?= $this->render('/site/sidebar', [
        'id' => $id,
    ]) ?>
    <div class="content" id="content">
        <div class="page-content">
            <?= UserFilterWidget::widget([
                'title' => 'Пользователи / Создание пользователя',
            ]) ?>
            <div class="form-wrapper">
                <?php $form = ActiveForm::begin([
                    'id' => 'user-form',
                    'options' => ['class' => 'form-valid'],
                ]); ?>

                <div class="form-header">
                    Создание пользователя
                </div>
                <div class="form-group">
                    <div class="label">Введите Логин</div>
                    <input type="text" name="User[username]">
                </div>
                <div class="form-group">
                    <div class="label">Введите пароль</div>
                    <input type="text" name="User[password]">
                </div>
                <div class="form-group">
                    <div class="label">ФИО сотрудника</div>
                    <input type="text" name="User[surname_name]">
                </div>
                <div class="form-group">
                    <div class="label">Права доступа</div>
                    <select name="User[access]" class="select2-single">
                        <?php foreach (User::$access as $id => $item): ?>
                            <option value="<?= $id ?>"><?= $item ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <div class="label">Округ</div>
                    <select name="User[area_id]" class="select2-single">
                        <option value="0" selected>Все</option>
                        <?php foreach ($areas as $id => $item): ?>
                            <option value="<?= $id ?>" <?= $model->area_id == $id ? ' selected' : '' ?>><?= $item->name ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <div class="label">Магазин</div>
                    <select name="User[store_id]" class="select2-single">
                        <option value="0" selected>Все</option>
                        <?php foreach ($stores as $id => $item): ?>
                            <option value="<?= $id ?>" <?= $model->store_id == $id ? ' selected' : '' ?>><?= $item->name ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <div class="label">Введите email</div>
                    <input type="email" name="User[email]">
                </div>
                <div class="form-group">
                    <div class="label">Введите телефон</div>
                    <input type="text" name="User[phone]">
                </div>
                <div class="form-group check-group">
                    <label>
                        <input type="checkbox" name="User[status]" <?= $model->status == USER::STATUS_ACTIVE ? ' checked' : '' ?>>
                        Активен
                    </label>
                </div>
                <div class="btn-wrap">
                    <button class="btn-default">Отправить</button>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>