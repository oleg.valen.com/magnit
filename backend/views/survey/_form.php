<?php

//use yii\bootstrap4\ActiveForm;
use backend\views\widgets\UserFilterWidget;
use yii\helpers\Url;
use common\models\User;
use common\models\Survey;
use yii\widgets\ActiveForm;
use common\models\Log;
use yii\helpers\Html;
use common\models\SurveyType;

?>

<div class="container">
    <div class="header">
        <div class="navbar-aside">
            <div class="mobile-menu btn-aside-menu toggle-sidebar">
                <div class="b-menu">
                    <div class="b-bun b-bun--top"></div>
                    <div class="b-bun b-bun--mid"></div>
                    <div class="b-bun b-bun--bottom"></div>
                </div>
            </div>
        </div>
    </div>
    <?= $this->render('/site/sidebar', [
        'id' => $id,
    ]) ?>
    <div class="content" id="content">
        <div class="page-content">
            <div class="one-anket-header">
                <?= UserFilterWidget::widget([
                    'title' => 'Все анкеты / Профиль клиента',
                    'model' => $model,
                ]) ?>
            </div>
            <div class="client-answer-container">
                <p class="answer-title">Ответы клиента</p>
                <?php foreach (Survey::$surveys[$model->survey_type_id] as $key => $value): ?>
                    <div class="answer-item">
                        <span class="q-text"><?= $key ?>. <?= $value ?></span>
                        <div class="answer-box">
                            <?php if ($model->survey_type_id != SurveyType::SERVICE): ?>
                                <p class="answer-txt"><?= $model['question' . $key] ?></p>
                            <?php else: ?>
                                <p class="answer-txt"><?= $model['question' . $key] == 1 ? 'да' : 'нет' ?></p>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endforeach; ?>
                <div class="answer-item">
                    <span class="q-text">Если у Вас есть предложения, просьба оставить их здесь</span>
                    <div class="answer-box">
                        <p class="answer-txt"><?= Html::encode($model->comment) ?></p>
                    </div>
                </div>
                <div class="images-container">
                    <?php foreach ($images as $item): ?>
                        <div class="img-item">
                            <img src="/uploads/<?= $item ?>" alt="">
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>

            <?php $form = ActiveForm::begin([
                'id' => 'survey-update-form',
            ]) ?>
            <input type="hidden" name="mode" value="">
            <?php if ($logs): ?>
                <div class="alert-history-container">
                    <p class="answer-title">История обработки Алерта</p>
                    <?php foreach ($logs as $item): ?>
                        <div class="history-group">
                            <div class="history-item">
                                <p class="history-item-name">Время события:</p>
                                <p class="history-item-data"><?= date('d.m.Y H:m:s', $item->created_at) ?></p>
                            </div>
                            <div class="history-item">
                                <p class="history-item-name">ФИО:</p>
                                <p class="history-item-data"><?= $item->user->surname_name ?></p>
                            </div>
                            <div class="history-item">
                                <p class="history-item-name">Действие:</p>
                                <p class="history-item-data"><?= Log::$statuses[$item->status] ?></p>
                            </div>
                            <?php if (!empty($item->comment)): ?>
                                <div class="history-item">
                                    <p class="history-item-name">Комментарий о выполненой работе:</p>
                                    <p class="history-item-data alert-comment"><?= $item->comment ?></p>
                                </div>
                            <?php endif; ?>
                        </div>
                    <?php endforeach; ?>
                    <?php if (false): ?>
                        <div class="history-item">
                            <p class="history-item-name">Комментарий о выполненой работе:</p>
                            <p class="history-item-data alert-comment">Спасибо, что приняли участие в опросе об
                                уровне
                                нашего сервиса. Мы всегда стремимся к высокому уровню обслуживания клиентов и нам
                                очень
                                жаль, что вы не увидели заинтересованности от наших сотрудников</p>
                        </div>
                    <?php endif; ?>
                </div>
            <?php endif; ?>

            <?php if (Yii::$app->user->can('viewGM')): ?>
                <?php if ($model->status == Survey::STATUS_NEW
                    || $model->status == Survey::STATUS_GM_EXPIRED
                    || $model->status == Survey::STATUS_BF_REVISION
                ): ?>
                    <div class="btn-wrapper take-to-work">
                        <button type="button" class="anket-btn">Взять в работу</button>
                    </div>
                <?php elseif ($model->status == Survey::STATUS_GM_TAKEN): ?>
                    <div class="btn-wrapper complete-btn-wrapper" style="display: block">
                        <button type="button" class="anket-btn">Завершить выполнение</button>
                    </div>
                <?php endif; ?>
                <div class="comment-container">
                    <p>Комментарий сотрудника</p>
                    <textarea class="comment-field" name="comment"></textarea>
                    <span class="comment-error">Поле обязательно к заполнению</span>
                    <div class="send-btn-wrp">
                        <button type="submit" class="send-btn">Отправить</button>
                    </div>
                </div>
            <?php endif; ?>
            <?php if (Yii::$app->user->can('viewBF') && $model->status == Survey::STATUS_GM_CLOSED): ?>
                <div class="comment-container" style="display: block">
                    <div class="btns-row">
                        <div class="btn-wrapper accepted" data-mode="accepted">
                            <button type="button" class="anket-btn">Принять</button>
                        </div>
                        <div class="btn-wrapper revision" data-mode="revision">
                            <button type="button" class="anket-btn">На доработку</button>
                        </div>
                    </div>
                    <div class="revision-comment" style="display: none">
                        <p>Комментарий</p>
                        <textarea class="comment-field" name="revision-comment"></textarea>
                    </div>
                    <div class="btn-wrapper confirmation-send">
                        <button type="button" class="anket-btn">Отправить</button>
                    </div>
                </div>
            <?php endif; ?>

            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>