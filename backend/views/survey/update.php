<?php

use yii\helpers\Html;

?>
<div class="delivery-survey-update">

    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', [
        'id' => $id,
        'model' => $model,
        'logs' => $logs,
        'images' => $images,
    ]) ?>

</div>