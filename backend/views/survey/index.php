<?php

use yii\helpers\Url;
use yii\helpers\Html;
use backend\views\widgets\SurveyFilterWidget;
use common\models\Department;
use common\models\Survey;
use yii\widgets\LinkPager;
use common\models\SurveyType;
use yii\helpers\Json;
use yii\widgets\ActiveForm;

?>

<div class="container">
    <div class="header">
        <div class="navbar-aside">
            <div class="mobile-menu btn-aside-menu toggle-sidebar">
                <div class="b-menu">
                    <div class="b-bun b-bun--top"></div>
                    <div class="b-bun b-bun--mid"></div>
                    <div class="b-bun b-bun--bottom"></div>
                </div>
            </div>
        </div>
    </div>
    <?= $this->render('/site/sidebar', [
        'id' => $id,
    ]) ?>
    <div class="content" id="content">
        <div class="page-content">
            <?= SurveyFilterWidget::widget([
                'showAlerts' => true,
                'title' => 'Все анкеты',
                'filters' => $filters,
            ]) ?>
            <div class="search-row">
                <div class="search-wrap">
                    <?php $form = ActiveForm::begin([
                        'id' => 'search-form',
                        'options' => [],
                    ]); ?>
                    <?= $form->field($searchModel, 'createdAt')->hiddenInput(['value' => $filters['createdAt']])->label(false) ?>
                    <?= $form->field($searchModel, 'search')->label(false) ?>
                    <button class="search-submit"></button>
                    <?php ActiveForm::end(); ?>
                </div>
                <?php if (false): ?>
                    <a href="#" class="unload-wrap">
                        <div class="icon">
                        </div>
                        <div class="text">
                            <div class="big-text">
                                Выгрузка
                            </div>
                            <div class="small-text">
                                Скачать файл
                            </div>
                        </div>
                    </a>
                <?php endif ?>

                <?= Html::a('<div class="icon"></div><div class="text"><div class="big-text">Выгрузка</div><div class="small-text">Скачать файл</div></div>',
                    Url::to(['surveys/upload-xlsx']), [
                        'class' => 'unload-wrap',
                        'data-method' => 'post',
                        'data-params' => Json::encode([
                            'Form[createdAt]' => $filters['createdAt'],
                            'Form[storeId]' => $filters['storeId'],
                            'Form[departmentId]' => $filters['departmentId'],
                            'Form[surveyTypeId]' => $filters['surveyTypeId'],
                        ]),
                    ]) ?>

            </div>
            <div class="table-wrap">
                <div class="table-responsive">
                    <table>
                        <thead>
                        <tr>
                            <th>Индикатор<br>Алерта</th>
                            <th>Статус<br>Алерта</th>
                            <th>Номер<br>Алерта</th>
                            <th>Дата<br>опроса</th>
                            <th>Оценка<br></th>
                            <th>Магазин</th>
                            <th>Отдел</th>
                            <th>Опрос</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($surveys as $item): ?>
                            <tr class="<?= in_array($item->nps, Survey::BAD_NPS, true) ? 'alert-row' : '' ?>">
                                <td>
                                    <a href="<?= Url::to(['survey/update', 'survey_id' => $item->survey_id]) ?>">
                                        <?php if (in_array($item->nps, Survey::BAD_NPS, true)): ?>
                                            <div class="alert-label">!ALERT</div>
                                        <?php else: ?>
                                            <div>-</div>
                                        <?php endif; ?>
                                    </a>
                                </td>
                                <td>Новый</td>
                                <td><?= $item->survey_id ?></td>
                                <td><?= date('d.m.Y h:m', $item->created_at) ?></td>
                                <td><?= $item->question1 ?></td>
                                <td><?= $item->store->name ?></td>
                                <td><?= $item->department->name ?></td>
                                <td><?= SurveyType::$valueNames[$item->survey_type_id] ?></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <?php if (false): ?>
                <div class="pagination-wrap">
                    <ul class="pagination">
                        <li class="prev disabled"><span>Назад</span></li>
                        <li class="active"><a href="#" data-page="0">1</a></li>
                        <li><a href="#" data-page="1">2</a></li>
                        <li><a href="#" data-page="2">3</a></li>
                        <li><a href="#" data-page="3">4</a></li>
                        <li class="next"><a href="#" data-page="1">Далее</a></li>
                    </ul>
                </div>
            <?php endif; ?>
            <div class="pagination-wrap">
                <?= LinkPager::widget([
                    'pagination' => $pages,
                    'prevPageLabel' => 'Назад',
                    'nextPageLabel' => 'Далее',
                ]) ?>
            </div>
        </div>
    </div>
</div>
