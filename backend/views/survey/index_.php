<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\views\widgets\SurveyFilterWidget;
use yii\helpers\Url;
use common\models\Survey;
use yii\widgets\LinkPager;
use yii\helpers\Json;

?>

<div class="container">
    <div class="header">
        <div class="navbar-aside">
            <div class="mobile-menu btn-aside-menu toggle-sidebar">
                <div class="b-menu">
                    <div class="b-bun b-bun--top"></div>
                    <div class="b-bun b-bun--mid"></div>
                    <div class="b-bun b-bun--bottom"></div>
                </div>
            </div>
        </div>
    </div>
    <?= $this->render('/site/sidebar', [
        'id' => $id,
    ]) ?>
    <div class="content" id="content">
        <div class="page-content">
            <?= SurveyFilterWidget::widget([
                'showAlerts' => true,
                'title' => 'Опрос по доставке / Все анкеты',
                'filters' => $filters,
            ]) ?>
            <div class="search-row">
                <?php if (false): ?>
                    <button class="">
                        <?= Html::a('Создать', ['delivery-survey/create', 'token' => $token->token], ['class' => '']) ?>
                    </button>
                <?php endif; ?>
                <?php if (in_array(Yii::$app->user->id, Yii::$app->authManager->getUserIdsByRole('admin'))): ?>
                    <?php $form = ActiveForm::begin([
                        'action' => 'delivery-survey/upload',
                        'options' => ['enctype' => 'multipart/form-data'],
                    ]); ?>
                    <?= $form->field($uploadModel, 'xlsxFile')->fileInput() ?>
                    <button type="submit">Загрузить</button>
                    <?php ActiveForm::end() ?>
                    <?= Html::a("<button class=''>Отправить SMS</button>", ['delivery-survey/send-sms']); ?>
                <?php endif; ?>

                <?php if (false): ?>
                    <div class="search-wrap">
                        <form action="#">
                            <input type="text">
                            <button class="search-submit"></button>
                        </form>
                    </div>
                <?php endif; ?>
                <?php if (false): ?>
                    <a href="#" class="unload-wrap">
                        <div class="icon">
                        </div>
                        <div class="text">
                            <div class="big-text">
                                Выгрузка
                            </div>
                            <div class="small-text">
                                Скачать файл
                            </div>
                        </div>
                    </a>
                <?php endif; ?>

                <?= Html::a('<div class="icon"></div><div class="text"><div class="big-text">Выгрузка</div><div class="small-text">Скачать файл</div></div>',
                    ['delivery-survey/upload-xlsx'], [
                        'class' => 'unload-wrap',
                        'data-method' => 'post',
                        'data-params' => Json::encode([
                            'Form[deliveryDate]' => $filters['deliveryDate'],
                            'Form[transportCompanyId]' => $filters['transportCompanyId'],
                            'Form[statusDeliveryId]' => $filters['statusDeliveryId'],
                            'Form[timeRangeName]' => $filters['timeRangeName'],
                            'Form[factoryId]' => $filters['factoryId'],
                            'Form[areaId]' => $filters['areaId'],
                        ]),
                    ]) ?>

            </div>
            <div class="table-wrap">
                <div class="table-responsive">
                    <table>
                        <thead>
                        <tr>
                            <th>Индикатор<br>Алерта</th>
                            <th>Статус<br>Алерта</th>
                            <th>Номер <br>Алерта</th>
                            <th>Дата<br>опроса</th>
                            <th>Оценка <br>NPS</th>
                            <th class="long-col">Дата доставки</th>
                            <th>№ документа<br>CRM</th>
                            <th class="long-col">Статус доставки</th>
                            <th class="long-col">Наименование транспортной<br>компании</th>
                            <th>Временной диапазон<br>доставки</th>
                            <th>Название<br>почтового региона</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($deliverySurveys as $item): ?>
                            <tr class="<?= in_array($item->nps, Survey::BAD_NPS, true) ? 'alert-row' : '' ?>">
                                <td>
                                    <a href="<?= Url::to(['delivery-survey/update', 'survey_id' => $item->survey_id]) ?>">
                                        <?php if (in_array($item->nps, Survey::BAD_NPS, true)): ?>
                                            <div class="alert-label">!ALERT</div>
                                        <?php else: ?>
                                            <div>-</div>
                                        <?php endif; ?>
                                    </a>
                                </td>
                                <td><?= in_array($item->nps, Survey::BAD_NPS, true) ? Survey::$statusValues[$item->status] : '' ?></td>
                                <td><?= in_array($item->nps, Survey::BAD_NPS, true) ? $item->survey_id : '' ?></td>
                                <td><?= date('d.m.Y h:m', $item->created_at) ?></td>
                                <td><?= $item->nps ?></td>
                                <td><?= date('d.m.Y h:m', $item->deliveryDoc->delivery_date) ?></td>
                                <td><?= $item->deliveryDoc->numdoc_crm ?></td>
                                <td><?= $item->deliveryDoc->statusDelivery->name ?></td>
                                <td><?= $item->deliveryDoc->transportCompany->name ?></td>
                                <td><?= $item->deliveryDoc->time_range ?></td>
                                <td><?= $item->deliveryDoc->area->name ?></td>
                            </tr>

                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <?php if (false): ?>
                <div class="pagination-wrap">
                    <ul class="pagination">
                        <li class="prev disabled"><span>Назад</span></li>
                        <li class="active"><a href="#" data-page="0">1</a></li>
                        <li><a href="#" data-page="1">2</a></li>
                        <li><a href="#" data-page="2">3</a></li>
                        <li><a href="#" data-page="3">4</a></li>
                        <li class="next"><a href="#" data-page="1">Далее</a></li>
                    </ul>
                </div>
            <?php endif; ?>
            <div class="pagination-wrap">
                <?= LinkPager::widget([
                    'pagination' => $pages,
                ]) ?>
            </div>
        </div>
    </div>
</div>