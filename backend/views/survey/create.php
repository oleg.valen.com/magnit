<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

$this->params['breadcrumbs'] = [
    [
        'label' => 'Товары',
    ],
];

?>

<div class="page-wrapper">
    <div class="page-inner">
        <div class="top-panel">
            <div class="logo">
                <?= Html::img('@web/img/logo.png', ['alt' => 'reStore']) ?>
            </div>
        </div>
        <div class="survey-content survey-wrap">
            <?php $form = ActiveForm::begin([
                'id' => 'deliver-survey-form',
                //                'enableAjaxValidation' => true,
                //                'action' => Url::to(['anketa-product/index', 'anketa_id' => $anketa->anketa_id]),
                'options' => ['class' => 'form-valid'],
            ]); ?>
            <div class="first-question">
                <div class="first-question-content">
                    <div class="question">
                        Какая вероятность того, что Вы порекомендуете друзьям и знакомым совершить покупку техники в
                        магазине re:Store?
                    </div>
                    <div class="labels">
                        <div class="label">
                            Не порекомендую
                        </div>
                        <div class="label">
                            Точно порекомендую
                        </div>
                    </div>
                    <div class="range10">
                        <div class="answer">
                            <input type="radio" class="nps-btn" data-name="q-1-1" data-req=""
                                   id="q-range-1" name="DeliverySurvey[nps]"
                                   value="0">
                            <label for="q-range-1" class="btn btn-1">
                                <div class="range-number color1">0</div>
                            </label>
                        </div>
                        <div class="answer">
                            <input type="radio" class="nps-btn" data-name="q-1-1" data-req=""
                                   id="q-range-2" name="DeliverySurvey[nps]"
                                   value="1">
                            <label for="q-range-2" class="btn btn-1">
                                <div class="range-number color2">1</div>
                            </label>
                        </div>
                        <div class="answer">
                            <input type="radio" class="nps-btn" data-name="q-1-1" data-req=""
                                   id="q-range-3" name="DeliverySurvey[nps]"
                                   value="2">
                            <label for="q-range-3" class="btn btn-1">
                                <div class="range-number color3">2</div>
                            </label>
                        </div>
                        <div class="answer">
                            <input type="radio" class="nps-btn" data-name="q-1-1" data-req=""
                                   id="q-range-4" name="DeliverySurvey[nps]"
                                   value="3">
                            <label for="q-range-4" class="btn btn-1">
                                <div class="range-number color4">3</div>
                            </label>
                        </div>
                        <div class="answer">
                            <input type="radio" class="nps-btn" data-name="q-1-1" data-req=""
                                   id="q-range-5" name="DeliverySurvey[nps]"
                                   value="4">
                            <label for="q-range-5" class="btn btn-1">
                                <div class="range-number color5">4</div>
                            </label>
                        </div>
                        <div class="answer">
                            <input type="radio" class="nps-btn" data-name="q-1-1" data-req=""
                                   id="q-range-6" name="DeliverySurvey[nps]"
                                   value="5">
                            <label for="q-range-6" class="btn btn-1">
                                <div class="range-number color6">5</div>
                            </label>
                        </div>
                        <div class="answer">
                            <input type="radio" class="nps-btn" data-name="q-1-1" data-req=""
                                   id="q-range-7" name="DeliverySurvey[nps]"
                                   value="6">
                            <label for="q-range-7" class="btn btn-1">
                                <div class="range-number color7">6</div>
                            </label>
                        </div>
                        <div class="answer">
                            <input type="radio" class="nps-btn" data-name="q-1-1" data-req=""
                                   id="q-range-8" name="DeliverySurvey[nps]"
                                   value="7">
                            <label for="q-range-8" class="btn btn-1">
                                <div class="range-number color8">7</div>
                            </label>
                        </div>
                        <div class="answer">
                            <input type="radio" class="nps-btn" data-name="q-1-1" data-req=""
                                   id="q-range-9" name="DeliverySurvey[nps]"
                                   value="8">
                            <label for="q-range-9" class="btn btn-1">
                                <div class="range-number color9">8</div>
                            </label>
                        </div>
                        <div class="answer">
                            <input type="radio" class="nps-btn" data-name="q-1-1" data-req=""
                                   id="q-range-10" name="DeliverySurvey[nps]"
                                   value="9">
                            <label for="q-range-10" class="btn btn-1">
                                <div class="range-number color10">9</div>
                            </label>
                        </div>
                        <div class="answer">
                            <input type="radio" class="nps-btn" data-name="q-1-1" data-req=""
                                   id="q-range-11" name="DeliverySurvey[nps]"
                                   value="10">
                            <label for="q-range-11" class="btn btn-1">
                                <div class="range-number color11">10</div>
                            </label>
                        </div>
                    </div>
                    <div class="bottom-text">
                        <div class="text-to-range text-9-10">
                            <p>Благодарим Вас за высокую оценку!
                            <p>
                                Что вам понравилось больше всего?
                        </div>
                        <div class="text-to-range text-7-8">
                            <p>Спасибо за оценку!</p>
                            Что мы можем улучшить для того, чтобы Вы поставили нам более высокую
                        </div>
                        <div class="text-to-range text-0-6">
                            Очень сожалеем, что не оправдали Ваши ожидания.<br>
                            Что мы можем сделать, чтобы стать лучше?
                        </div>
                    </div>
                    <div class="comment-wrap hidden">
                        <textarea name="DeliverySurvey[comment]" placeholder="Ваш комментарий" rows="1"></textarea>
                    </div>
                    <div class="btn-wrap hidden-btn">
                        <div class="btn-default btn-pink show-minute">
                            Отправить
                        </div>
                    </div>
                </div>
            </div>
            <div class="purple-container minute-cont" style="display: none;">
                <div class="text-block">
                    Уделите, пожалуйста, еще <span class="green">1 минуту</span>, для оценки доставки.
                </div>
                <div class="btns-wrap">
                    <a href="#" class="btn-default show-survey">
                        Да, конечно
                    </a>
                    <button class="btn-default">
                        Нет, в другой раз
                    </button>
                </div>
            </div>
            <div class="deliver-survey" style="display: none;">
                <div class="question-list">
                    <div class="question-wrap">
                        <div class="top-row">
                            <div class="question">
                                Как вы оцениваете доставку товара?
                            </div>
                        </div>
                        <div class="answers-list">
                            <div class="answer-item">
                                <input type="radio" name="DeliverySurvey[question1]" id="q-1-1" value="5" data-reqired>
                                <label for="q-1-1">Отлично</label>
                            </div>
                            <div class="answer-item">
                                <input type="radio" name="DeliverySurvey[question1]" id="q-1-2" value="4" data-reqired>
                                <label for="q-1-2">Хорошо</label>
                            </div>
                            <div class="answer-item">
                                <input type="radio" name="DeliverySurvey[question1]" id="q-1-3" value="3" data-reqired>
                                <label for="q-1-3">Удовлетворительно</label>
                            </div>
                            <div class="answer-item">
                                <input type="radio" name="DeliverySurvey[question1]" id="q-1-4" value="2" data-reqired>
                                <label for="q-1-4">Плохо</label>
                            </div>
                            <div class="answer-item">
                                <input type="radio" name="DeliverySurvey[question1]" id="q-1-5" value="1" data-reqired>
                                <label for="q-1-5">Очень плохо</label>
                            </div>
                        </div>
                        <div class="error-text">
                            Выберите ответ
                        </div>
                    </div>
                    <div class="question-wrap">
                        <div class="top-row">
                            <div class="question">
                                Курьер приехал в согласованное время доставки?
                            </div>
                        </div>
                        <div class="answers-list">
                            <div class="answer-item">
                                <input type="radio" name="DeliverySurvey[question2]" id="q-2-1"
                                       value="1" data-reqired>
                                <label for="q-2-1">Да, день и время соответствуют указанным в заказе</label>
                            </div>
                            <div class="answer-item">
                                <input type="radio" name="DeliverySurvey[question2]" id="q-2-2"
                                       value="2" data-reqired>
                                <label for="q-2-2">Нет, приехал в назначенный день, но в другое время</label>
                            </div>
                            <div class="answer-item">
                                <input type="radio" name="DeliverySurvey[question2]" id="q-2-3" value="3"
                                       data-reqired>
                                <label for="q-2-3">Нет, приехал в другой день</label>
                            </div>
                        </div>
                        <div class="error-text">
                            Выберите ответ
                        </div>
                    </div>
                    <div class="question-wrap">
                        <div class="top-row">
                            <div class="question">
                                Как вы оцениваете сохранность упаковки самого товара?
                            </div>
                        </div>
                        <div class="answers-list">
                            <div class="answer-item">
                                <input type="radio" name="DeliverySurvey[question3]" id="q-3-1" value="1" data-reqired>
                                <label for="q-3-1">Всё хорошо</label>
                            </div>
                            <div class="answer-item">
                                <input type="radio" name="DeliverySurvey[question3]" id="q-3-2" value="2"
                                       data-reqired>
                                <label for="q-3-2">Не распаковывал(а)/Покупал(а) не себе</label>
                            </div>
                            <div class="answer-item">
                                <input type="radio" name="DeliverySurvey[question3]" id="q-3-3" value="3"
                                       data-reqired>
                                <label for="q-3-3">Упаковка в пыли/испачкана</label>
                            </div>
                            <div class="answer-item">
                                <input type="radio" name="DeliverySurvey[question3]" id="q-3-4" value="4" data-reqired>
                                <label for="q-3-4">Упаковка помята</label>
                            </div>
                        </div>
                        <div class="error-text">
                            Выберите ответ
                        </div>
                    </div>
                    <div class="question-wrap">
                        <div class="top-row">
                            <div class="question">
                                Курьер предупредил ли заранее о своем приезде?
                            </div>
                        </div>
                        <div class="answers-list">
                            <div class="answer-item">
                                <input type="radio" name="DeliverySurvey[question4]" id="q-4-1" value="1"
                                       data-reqired>
                                <label for="q-4-1">Позвонил не менее чем за полчаса</label>
                            </div>
                            <div class="answer-item">
                                <input type="radio" name="DeliverySurvey[question4]" id="q-4-2" value="2"
                                       data-reqired>
                                <label for="q-4-2">Курьер позвонил только по приезде</label>
                            </div>
                            <div class="answer-item">
                                <input type="radio" name="DeliverySurvey[question4]" id="q-4-3" value="3"
                                       data-reqired>
                                <label for="q-4-3">Курьер не предупредил о приезде</label>
                            </div>
                        </div>
                        <div class="error-text">
                            Выберите ответ
                        </div>
                        <div class="btn-wrap">
                            <button class="btn-default btn-submit btn-pink delivery-survey">
                                Отправить
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>