<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

?>

<div class="page-wrapper">
    <div class="inner-content">
        <div class="logo">
            <?= Html::img('@web/img/logo.svg', ['alt' => 'magnit-opros']) ?>
        </div>
        <div class="pool-header">
            Ваши контактные
            данные
        </div>
        <div class="form-wrap">
            <?php $form = ActiveForm::begin([
                'id' => 'client-form',
                'options' => ['class' => 'form-valid contact-form'],
            ]); ?>
            <?= $form->field($client, 'surveyId')->hiddenInput(['value' => $surveyId])->label(false); ?>
            <div class="form-group">
                <div class="input-text">
                    <input type="text" name="Client[name]" placeholder="Имя" data-reqired="reqired" class="name-input">
                </div>
                <div class="error-text">
                    Заполните поле
                </div>
            </div>
            <div class="form-group">
                <div class="phone-row">
                    <div class="input-text phone-code">
                        <input type="text" name="Client[phoneCode]" value="+7">
                    </div>
                    <div class="input-text phone-number">
                        <input type="tel" name="Client[phone]" placeholder="Мобильный" class="phone-input"
                               maxlength="11" data-reqired="reqired">
                    </div>
                </div>
                <div class="error-text">
                    Заполните поле
                </div>
            </div>
            <div class="form-group">
                <div class="input-text">
                    <input type="text" name="Client[email]" placeholder="Email" class="email-input"
                           data-reqired="reqired">
                </div>
                <div class="error-text">
                    Заполните поле
                </div>
            </div>
            <div class="form-group">
                <div class="check-row">
                    <input type="checkbox" name="Client[agree]" id="q-5" data-reqired="reqired">
                    <label for="q-5">
                        <div class="check"></div>
                        <span>
                                    Я согласен с правилами пользовательського соглашения
                                </span>
                    </label>
                </div>
            </div>
            <div class="btn-wrap">
                <button class="btn-submit inactive">Отправить</button>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>