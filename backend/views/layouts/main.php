<?php

/* @var $this \yii\web\View */

/* @var $content string */

use common\widgets\Alert;
use backend\assets\AppAsset;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>" class="h-100">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <?php $this->registerCsrfMetaTags() ?>
        <title><?= Html::encode('Magnit') ?></title>
        <?php $this->head() ?>
    </head>

    <body>
    <?php $this->beginBody() ?>

    <div class="container">
        <div class="header">
            <div class="navbar-aside">
                <div class="mobile-menu btn-aside-menu toggle-sidebar">
                    <div class="b-menu">
                        <div class="b-bun b-bun--top"></div>
                        <div class="b-bun b-bun--mid"></div>
                        <div class="b-bun b-bun--bottom"></div>
                    </div>
                </div>
            </div>
        </div>
        <?= $content ?>
    </div>
    <footer></footer>
    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage();


